<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

/**
 * A class used to calculate the results for Alternative voting.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class question {
    /** @var mod_vote_user[] $user_array - An array of users who voted. */
    protected $userarray = [];

    /** @var array - An array of user's option. */
    protected $optionlist = [];

    /**
     * Adds a user object to the question, if they have not already been added.
     *
     * @param mod_vote_user $user
     */
    public function add_user(user &$user) {
        if (!in_array($user, $this->userarray)) {
            $this->userarray[] =& $user;
        }
    }

    /**
     * Removes the option from all the users on the question.
     *
     * @param int $optionid - The id of an option to remove.
     */
    public function remove_option($optionid) {
        $key = array_search($optionid, $this->optionlist);

        if ($key !== false) { // Then we found the option, so we need to remove it.
            unset($this->optionlist[$key]);
        }

        foreach ($this->userarray as $user) {
            $user->remove_option($optionid);
        }
    }

    /**
     * Store the option so that we can give it 0 votes if no one has voted for it.
     *
     * @param int $optionid
     */
    public function add_option($optionid) {
        if (!in_array($optionid, $this->optionlist)) {
            $this->optionlist[] = $optionid;
        }
    }

    /**
     * Create a score for an option to evaluate how popular it is overal,
     * higher ranked choices should have a much greater affect than lower ranked choices.
     *
     * @param int $optionid - The id of the option to score.
     * @return float
     */
    public function score_option($optionid) {
        $score = 0;
        foreach ($this->userarray as $user) {
            $score += $user->score_option($optionid);
        }
        return $score;
    }

    /**
     * Returns an array where each key is an option id and the value is the number of votes for the option.
     * If no results are found it will return an empty array.
     *
     * @return array
     */
    public function get_results() {
        $output = [];

        foreach ($this->optionlist as $option) {
            $output[$option] = 0;
        }

        foreach ($this->userarray as $user) {
            $optionid = $user->get_choice();
            if ($optionid !== false) {
                if (isset($output[$optionid])) { // The option has been found for another user so increment it.
                    $output[$optionid]++;
                } else { // This is the first time the option has been found.
                    $output[$optionid] = 1;
                }
            }
        }

        return $output;
    }
}
