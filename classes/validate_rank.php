<?php
// This file is part of the timetable import block
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

/**
 * This class is used to validate that a user has submitted valid data in a AV type vote.
 *
 * Note: changes to this class may also require corresponding changes in the validateOptions() function
 * of the question controller in the Mobile plugin to ensure both validate in the same way.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class validate_rank {
    /**
     * @var int[] $ranks - An array that stores the options a user submitted.
     *                     The key is the rank they assigned, the value the id of the option.
     */
    protected $ranks = [];

    /**
     * Checks to see if the rank has already been used, it returns false if it has and true if it has not.
     *
     * @param int $rank - The rank a user gave an option
     * @param int $option - an option id
     * @return boolean
     */
    public function add($rank, $option) {
        if ($rank == 0) {
            return true;
        } else if (isset($this->ranks[$rank])) { // The rank already has a value set.
            return false;
        } else {
            $this->ranks[$rank] = $option;
            return true;
        }
    }

    /**
     * Checks that the ranks submitted are consectutive and start at 1.
     *
     * @return boolean
     */
    public function validate() {
        $stopat = count($this->ranks) + 1;
        // Check that all the options are numerically present.
        for ($i = 1; $i < $stopat; $i++) {
            if (!isset($this->ranks[$i])) {
                return false;
            }
        }
        // If it gets here it must be valid.
        return true;
    }
}
