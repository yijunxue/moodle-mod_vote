<?php
// This file is part of the vote plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//

namespace mod_vote\external;

use core_external\external_function_parameters;
use core_external\external_value;
use core_external\external_warnings;
use mod_vote\event\vote_viewed;

/**
 * Defines the web service endpoint for registering a view of a Vote activity.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class view extends \core_external\external_api {
    /**
     * Logs that a user viewed a Vote activity.
     *
     * @param int $id The id of a vote activity.
     * @return array
     */
    public static function execute($id) {
        global $DB;
        // Get the vote activity and check that the user should have access to it.
        $vote = $DB->get_record('vote', ['id' => $id], '*', MUST_EXIST);
        list($course, $cm) = get_course_and_cm_from_instance($vote, 'vote');
        $context = \context_module::instance($cm->id);
        self::validate_context($context);
        $warnings = [];
        // Mark viewed if required.
        $completion = new \completion_info($course);
        $completion->set_module_viewed($cm);
        // Trigger a view event.
        $eventdata = [
            'context' => $context,
            'objectid' => $vote->id,
        ];
        $event = vote_viewed::create($eventdata);
        $event->add_record_snapshot('course_modules', $cm);
        $event->add_record_snapshot('course', $course);
        $event->add_record_snapshot('vote', $vote);
        $event->trigger();
        // Generate the result.
        $result = [];
        $result['status'] = true;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Defines the inputs for the web service method.
     *
     * @return \core_external\external_function_parameters
     */
    public static function execute_parameters() {
        return new external_function_parameters([
            'id' => new external_value(PARAM_INT, 'The instance id of a Vote activity', VALUE_REQUIRED),
        ]);
    }

    /**
     * Defines the output of the web service.
     *
     * @return \core_external\external_function_parameters
     */
    public static function execute_returns() {
        return new external_function_parameters(
            [
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings(),
            ]
        );
    }
}
