<?php
// This file is part of the timetable import block
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

use core_external\util;

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__DIR__) . '/lib.php');

/**
 * This class holds the information for a vote.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2014 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class vote {
    /** @var int $_closedate - UNIX timestamp for the close date of the vote. */
    protected $_closedate;

    /** @var bool $_closed - true when the votes close date has passed, false when it has not. */
    protected $_closed;

    /** @var stdClass $_cm - The Moodle course_module record for the vote activity. */
    protected $_cm;

    /** @var stdClass $_course - The database record for the course the vote activity is part of.   */
    protected $_course;

    /** @var int $_id - The id of the vote activity. */
    protected $_id;

    /** @var string $_intro - The description of the vote activity. */
    protected $_intro;

    /** @var int $_introformat - The format of the intro. */
    protected $_introformat;

    /** @var string $_name - The name of the vote. */
    protected $_name;

    /** @var int $_votestate - Stores if the vote is active or in editing mode. */
    protected $_votestate;

    /** @var int $_votetype - Stores if the vote is a poll, vote or AV. */
    protected $_votetype;

    /** @var bool $_completionvoted - If true then students should vote to complete the vote. */
    protected $_completionvoted;

    /** @var bool $canedit - True if the user has editing rights to the vote. */
    private $canedit;

    /** @var bool $cansubmit - True if the user is able to submit. */
    private $cansubmit;

    /**
     * @var stdClass[] $questions - Stores an array of the questions and options
     * that are part of the vote, used to render the edit page
     */
    protected $questions;

    /**
     * @var stdClass[] $results - Stores and array of questions with the results
     * for each option, the information is used to render the results.
     */
    protected $results;

    /** @var bool $hasvoted - Stores if the user has submitted a vote for the vote activity */
    private $hasvoted;

    /**
     * Create an instance of mod_vote_renderable.
     *
     * @param int|stdClass $vote The database id of the vote, or a single record returned by get_all_instances_in_courses for it.
     * @throws coding_exception
     */
    public function __construct($vote) {
        global $DB;

        if (is_a($vote, 'stdClass') && !empty($vote->coursemodule)) {
            // A record from get_all_instances_in_courses() was passed.
            $this->_id = $vote->id;
            $this->_name = $vote->name;
            $this->_votetype = $vote->votetype;
            $this->_closedate = $vote->closedate;
            $this->_closed = ($vote->closedate < time());
            $this->_votestate = $vote->votestate;
            $this->_intro = $vote->intro;
            $this->_introformat = $vote->introformat;
            $this->_completionvoted = $vote->completionvoted;
            // The mod_vote_renderable object needs the id to be the course_module's id,
            // get_all_instances_in_courses() sets the id as the vote activity record id
            // and stored the course_module id as coursemodule. So we overwrite id with
            // coursemodule.
            // This should fix MOODLE-1554 Error in getting context in vote_print_overview().
            $vote->id = $vote->coursemodule;
            $this->_cm = $vote;
        } else if (!is_a($vote, 'stdClass') && (string)$vote === (string)intval($vote)) { // The value of $vote is an integer.
            $voterecord = $DB->get_record('vote', ['id' => $vote],
                'id, name, course, votetype, closedate, votestate, intro, introformat, completionvoted', MUST_EXIST);
            $this->_id = $vote;
            $this->_name = $voterecord->name;
            $this->_votetype = $voterecord->votetype;
            $this->_closedate = $voterecord->closedate;
            $this->_closed = ($voterecord->closedate < time());
            $this->_votestate = $voterecord->votestate;
            $this->_intro = $voterecord->intro;
            $this->_introformat = $voterecord->introformat;
            $this->_completionvoted = $voterecord->completionvoted;

            list($this->_course, $this->_cm) = get_course_and_cm_from_instance($vote, 'vote');
        } else {
            throw new \coding_exception('invalid paramter', 'An integer or a course_module record should be passed');
        }
    }

    /**
     * Magic method to return values of private and protected properties.
     *
     * @param string $name - The name of the property a value should be returned for.
     * @return mixed - Returns the value of the property, or null if it does not exist.
     */
    public function __get($name) {
        if ($name === 'questions') {
            return $this->get_questions();
        }

        if (property_exists($this, '_'.$name)) {
            return $this->{'_'.$name};
        }
        debugging('Invalid mod_vote/vote property accessed! '.$name);
        return null;
    }

    /**
     * Magic method to test if a private and protected variable isset().
     *
     * @param string $name - The name of the property that should be rested.
     * @return boolean
     */
    public function __isset($name) {
        if (property_exists($this, '_'.$name)) {
            return isset($this->{'_'.$name});
        }
        return false;
    }

    /**
     * Gets the renderable for the vote.
     *
     * @return \mod_vote\output\vote
     */
    public function get_renderable(): \mod_vote\output\vote {
        $vote = $this->get_base_renderable();
        $vote->intro = format_module_intro('vote', $this, $this->_cm->id);
        return $vote;
    }

    /**
     * Generates a vote renderable with all the base variables set.
     *
     * Methods using this will need to set all the variables that are not
     * common between the different output styles.
     *
     * @return \mod_vote\output\vote
     */
    protected function get_base_renderable(): \mod_vote\output\vote {
        $vote = new \mod_vote\output\vote();
        $vote->id = $this->_id;
        $vote->canedit = $this->can_edit();
        $vote->canvote = $this->can_submit();
        $vote->closed = ($this->_closedate < time());
        $vote->closedate = $this->_closedate;
        $vote->cmid = $this->_cm->id;
        $vote->displayresults = $this->results_visible();
        $vote->hasvoted = $this->has_voted();
        $vote->name = $this->_name;
        $vote->isaltvote = ($this->_votetype == VOTE_TYPE_AV);
        $vote->ispoll = ($this->_votetype == VOTE_TYPE_POLL);
        $vote->isvote = ($this->_votetype == VOTE_TYPE_VOTE);
        $vote->open = ($this->_votestate == VOTE_STATE_ACTIVE);
        $vote->type = $this->_votetype;
        if ($vote->displayresults) {
            $vote->questions = $this->get_result_renderables();
        } else {
            $vote->questions = $this->get_question_renderables();
        }
        return $vote;
    }

    /**
     * Gets the renderable for the vote with mobile formatting.
     *
     * @return \mod_vote\output\vote
     */
    public function get_renderable_mobile(): \mod_vote\output\vote {
        $vote = $this->get_base_renderable();
        $context = \context_module::instance($this->_cm->id);
        list($vote->intro, $summaryformat) =
                util::format_text($this->_intro, $this->_introformat, $context, 'mod_vote', 'intro');
        return $vote;
    }

    /**
     * Generates renderables for the questions in the vote, when editing mode is on.
     *
     * @return array
     */
    protected function get_question_renderables(): array {
        if (!isset($this->questions)) {
            $this->load_questions();
        }
        $return = [];
        foreach ($this->questions as $rawquestion) {
            $question = new \mod_vote\output\question();
            $question->id = $rawquestion->id;
            $question->text = $rawquestion->question;
            // Make the option renderables.
            $optionpos = 1;
            foreach ($rawquestion->options as $rawoption) {
                $option = new \mod_vote\output\option();
                $option->id = $rawoption->id;
                $option->text = $rawoption->name;
                $option->position = $optionpos++;
                $question->options[] = $option;
            }
            $return[] = $question;
        }
        return $return;
    }

    /**
     * Generates renderables for the questions in the vote, when results should be displayed.
     *
     * @return array
     */
    protected function get_result_renderables(): array {
        if (!isset($this->results)) {
            $this->load_results();
        }
        $return = [];
        foreach ($this->results as $questionid => $rawquestion) {
            $question = new \mod_vote\output\question();
            $question->id = $rawquestion->id;
            $question->text = $rawquestion->question;
            $maxrounds = $rawquestion->rounds;
            $maxresult = $rawquestion->maxresult;
            $question->maxresult = $maxresult;
            $question->rounds = $maxrounds;
            // Make the option renderables.
            $optionpos = 1;
            foreach ($rawquestion->options as $rawoption) {
                $option = new \mod_vote\output\option();
                $option->id = $rawoption->id;
                $option->text = $rawoption->name;
                $option->position = $optionpos++;
                $round = $rawoption->round;
                $result = $rawoption->result;
                if ($round < $maxrounds) {
                    $option->round = $round;
                }
                $option->votes = $result;
                if ($maxresult > 0) {
                    $option->percentage = ($result / $maxresult * 100);
                }
                $question->options[] = $option;
            }
            $return[] = $question;
        }
        return $return;
    }

    /**
     * Returns information about the questions and options that are part of the vote activity, including:
     * - id: the database id of the question
     * - question: the text of the question
     * - options: a stdClass[] of the options for that question
     *
     * Each option has the following information:
     * - id: the database id fo the option
     * - name: the options text
     *
     * @return stdClass[]
     */
    public function get_questions() {
        if (!isset($this->questions)) {
            $this->load_questions();
        }
        // Remove the question and option keys.
        $return = [];
        foreach ($this->questions as $question) {
            $tempoptions = [];
            foreach ($question->options as $option) {
                $tempoptions[] = $option;
            }
            $tempquestion = clone $question;
            $tempquestion->options = $tempoptions;
            $return[] = $tempquestion;
        }
        return $return;
    }

    /**
     * Loads the questions for the vote activity from the database.
     *
     */
    protected function load_questions() {
        global $DB;
        $this->questions = [];

        $params['voteid'] = $this->_id;
        $fields  = 'q.id AS questionid, q.question, o.id AS optionid, o.optionname';
        $sql = "SELECT $fields FROM {vote_question} q "
                ."LEFT JOIN {vote_options} o ON (q.id = o.questionid) "
                ."WHERE q.voteid = :voteid "
                ."ORDER BY q.sortorder, q.question, o.sortorder, o.optionname";

        $records = $DB->get_recordset_sql($sql, $params);
        foreach ($records as $record) {
            if (!isset($this->questions[$record->questionid])) {
                // We had not seen a record for this question before, so create it.
                $this->questions[$record->questionid] = new \stdClass();
                $this->questions[$record->questionid]->id = $record->questionid;
                $this->questions[$record->questionid]->question = $record->question;
                $this->questions[$record->questionid]->options = [];
            }
            if (!is_null($record->optionid)) {
                // Create the option.
                $tempoption = new \stdClass();
                $tempoption->id = $record->optionid;
                $tempoption->name = $record->optionname;
                $this->questions[$record->questionid]->options[$record->optionid] = $tempoption;
            }
        }
        $records->close();
    }

    /**
     * Returns an array of information suitable for rendering the results each array element has the following information:
     * - id: the database id of the question
     * - question: the text of the question
     * - rounds: the number of rounds of voting that took place
     * - maxresult: the highest number of votes an option gained
     * - options: a stdClass[] of the options and their results
     *
     * The option stdClass contains:
     * - id: the database id of the option
     * - name: the text of the option
     * - result: the final number of votes for the option
     * - round: the highest round of voting the option reached
     *
     * @return stdClass[]
     */
    public function get_results() {
        if (!isset($this->results)) {
            $this->load_results();
        }
        // Remove the question and option keys.
        $return = [];
        foreach ($this->results as $question) {
            $tempoptions = [];
            foreach ($question->options as $option) {
                $tempoptions[] = $option;
            }
            $tempquestion = clone $question;
            $tempquestion->options = $tempoptions;
            $return[] = $tempquestion;
        }
        return $return;
    }

    /**
     * Loads the results for the vote activity.
     */
    protected function load_results() {
        $this->results = [];

        $results = cachelib::get_cached_results($this);
        foreach ($results as $result) {
            if (!isset($this->results[$result->qid])) {
                $this->results[$result->qid] = new \stdClass();
                $this->results[$result->qid]->id = $result->qid;
                $this->results[$result->qid]->question = $result->question;
                $this->results[$result->qid]->rounds = 0;
                $this->results[$result->qid]->maxresult = 0;
                $this->results[$result->qid]->options = [];
            }

            // Create the option.
            $tempoptionresult = new \stdClass();
            $tempoptionresult->id = $result->oid;
            $tempoptionresult->name = $result->optionname;
            $tempoptionresult->result = $result->result;
            if ($result->result > $this->results[$result->qid]->maxresult) {
                // We have found a higher result.
                $this->results[$result->qid]->maxresult = $result->result;
            }
            $tempoptionresult->round = $result->round;
            if ($result->round > $this->results[$result->qid]->rounds) {
                // We have found a higher number of rounds.
                $this->results[$result->qid]->rounds = $result->round;
            }
            $this->results[$result->qid]->options[$result->oid] = $tempoptionresult;
        }
    }

    /**
     * Checks if the user has submitted a vote.
     *
     * @param int $userid The id of the user to be checked (defaults to the current user)
     * @return bool
     */
    public function has_voted($userid = null) {
        global $DB, $USER;
        if (empty($userid)) {
            $userid = $USER->id;
        }
        if ($userid != $USER->id || !isset($this->hasvoted)) {
            $hasvoted = $DB->record_exists('vote_votes', ['voteid' => $this->_id, 'userid' => $userid]);
            if ($userid == $USER->id) {
                // Only cache for the logged in user.
                $this->hasvoted = $hasvoted;
            }
        }
        return (isset($hasvoted)) ? $hasvoted : $this->hasvoted;
    }

    /**
     * Store the fact that the user has voted
     */
    public function set_voted() {
        $this->hasvoted = true;
    }

    /**
     * Checks if the user can submit a vote, has not already voted and that the close date has not passed.
     *
     * @param int $userid The id of the user to check the permission to submit for (defaults to the current user)
     * @return bool
     */
    public function can_submit($userid = null) {
        global $USER;
        if (empty($userid)) {
            $userid = $USER->id;
        }
        if ($userid != $USER->id || !isset($this->cansubmit)) {
            $submitcap = has_capability('mod/vote:submit', \context_module::instance($this->_cm->id), $userid);
            $cansubmit = (!$this->has_voted($userid) && $submitcap && !$this->_closed && $this->is_available());
            if ($userid == $USER->id) {
                // We should only cache the result for the logged in user.
                $this->cansubmit = $cansubmit;
            }
        }
        return (isset($cansubmit)) ? $cansubmit : $this->cansubmit;
    }

    /**
     * Checks if the user is able to edit the vote.
     *
     * @return bool
     */
    public function can_edit() {
        if (!isset($this->canedit)) {
            $this->canedit = has_capability('mod/vote:edit', \context_module::instance($this->_cm->id));
        }
        return $this->canedit;
    }

    /**
     * Tests is the vote is available to users for voting.
     *
     * @return bool
     */
    public function is_available(): bool {
        return $this->_votestate == VOTE_STATE_ACTIVE;
    }

    /**
     * Checks if the results should be shown to the user.
     *
     * @return bool
     */
    public function results_visible() {
        // Based on the vote type determins if the results should be diapalyed.
        switch ($this->_votetype) {
            case VOTE_TYPE_POLL: // With the poll we want to show the results.
                return ($this->has_voted() || $this->_closed);
            case VOTE_TYPE_VOTE:
            case VOTE_TYPE_AV:
                return $this->_closed;
        }

        return false; // Do not shoew results if the vote type is invalid.
    }
}
