<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderable question object.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_vote\output;

/**
 * Renderable question object.
 *
 * @package    mod_vote
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class question implements \renderable, \templatable {
    /** @var int The id of the question. */
    public $id;

    /** @var string The question's text. */
    public $text;

    /**@var \mod_vote\output\option[] The responses the question has. */
    public $options = [];

    /** @var int The number of rounds of vote counting that were required. */
    public $rounds = 0;

    /** @var int The most votes obtained by an option on the question. */
    public $maxresult = 0;

    /**
     * Exports the data for use in a template.
     *
     * @param \renderer_base $output
     * @return \stdClass
     */
    public function export_for_template(\renderer_base $output): \stdClass {
        $data = (object) [
            'qid' => $this->id,
            'text' => $this->text,
            'rounds' => $this->rounds,
            'maxresult' => $this->maxresult,
            'options' => [],
        ];
        foreach ($this->options as $option) {
            $data->options[] = $option->export_for_template($output);
        }
        return $data;
    }
}
