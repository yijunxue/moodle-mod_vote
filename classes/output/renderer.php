<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote\output;

use core\output\notification;
use mod_vote\vote;

/**
 * Defines the renderer for the Vote module.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class renderer extends \plugin_renderer_base {
    /**
     * Generate the string for an activity that will appear on the course overview.
     *
     * This is only used to display in data in the old style course overview which is deprecated,
     * it should not be used for new functionality.
     *
     * @param \mod_vote\vote $vote
     * @return string
     * @deprecated since version 2.9.4
     */
    public function render_overview(vote $vote) {
        $url = new \moodle_url('/mod/vote/view.php', ['id' => $vote->cm->coursemodule]);
        $link = \html_writer::link($url, $vote->name);
        $output = \html_writer::div(get_string('overviewname', 'mod_vote', ['link' => $link]), 'name');
        $output .= \html_writer::div(
                get_string('overviewmessage', 'mod_vote', ['closedate' => userdate($vote->closedate)]), 'info');
        return \html_writer::div($output, 'vote overview');
    }

    /**
     * Render a vote renderable.
     *
     * @param \mod_vote\output\vote $vote
     * @return string
     */
    public function render_vote(\mod_vote\output\vote $vote) {
        $data = $vote->export_for_template($this);
        return $this->render_from_template('mod_vote/vote', $data);
    }

    /**
     * Renders a vote form.
     *
     * @param \mod_vote\output\form $form
     * @return string
     */
    public function render_form(form $form) {
        return $this->render_from_template('mod_vote/form', $form->export_for_template($this));
    }
}
