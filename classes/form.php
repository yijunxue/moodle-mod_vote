<?php
// This file is part of the timetable import block
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

/**
 * Functions and objects used to vote in a vote module.
 *
 * @package    mod_vote
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class form extends \moodleform {
    /**
     * Define this form
     */
    public function definition() {
        global $DB, $OUTPUT;
        $mform =& $this->_form;

        $id = $this->_customdata['id'];
        $voteid = $this->_customdata['v'];
        $votetype = $this->_customdata['vt'];

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);
        $mform->setConstant('id', $id);

        $mform->addElement('hidden', 'voteid', null);
        $mform->setType('voteid', PARAM_INT);
        $mform->setConstant('voteid', $voteid);

        $mform->addElement('hidden', 'f', null);
        $mform->setType('f', PARAM_INT);
        $mform->setConstant('f', VOTE_FUNC_VOTE);

        $mform->addElement('hidden', 'vt', null);
        $mform->setType('vt', PARAM_INT);
        $mform->setConstant('vt', $votetype);

        // Get the number of options for all of the questions in this vote.
        $params['voteid'] = $voteid;

        $fields = 'q.id, COUNT(o.id) AS options';
        $sql = "SELECT $fields FROM {vote} v "
                ."JOIN {vote_question} q ON (v.id = q.voteid) "
                ."JOIN {vote_options} o ON (v.id = o.voteid AND q.id = o.questionid) "
                ."WHERE v.id = :voteid GROUP BY q.id "
                ."ORDER BY q.sortorder, q.question";
        $rs = $DB->get_recordset_sql($sql, $params);
        $optioncount = [];
        foreach ($rs as $record) {
            $optioncount[$record->id] = $record->options;
        }
        $rs->close();

        // Get a list of all questions that have options for this vote.
        $fields  = 'q.id AS qid, q.question, o.id AS id, o.optionname';
        $sql = "SELECT $fields FROM {vote} v "
                ."JOIN {vote_question} q ON (v.id = q.voteid) "
                ."JOIN {vote_options} o ON (v.id = o.voteid AND q.id = o.questionid) "
                ."WHERE v.id = :voteid "
                ."ORDER BY q.sortorder, q.question, o.sortorder, o.optionname";

        $rs = $DB->get_recordset_sql($sql, $params);
        $questionid = null;
        $optionarray = [];
        $firstoption;

        // Start processing the options into a form.
        foreach ($rs as $record) {
            if ($questionid != $record->qid) { // We have changed to a new question.
                switch ($votetype) {
                    case VOTE_TYPE_AV:
                        $optionarray = [0 => ''];
                        for ($i = 0; $i < $optioncount[$record->qid]; $i++) {
                            $optionarray[] = $i + 1;
                        }
                        break;
                    default:
                        if ($questionid != null) {
                            $mform->addGroup($optionarray, "group_question$questionid", '',
                                [\html_writer::empty_tag('br')], false);

                            $mform->setDefault("question$questionid", $firstoption);
                        }
                        $optionarray = [];
                        break;
                }
                $mform->addElement('header', 'headerquestion'.$record->qid, $record->question);

                $questionid = $record->qid;
                $questionname = $record->question;
                $firstoption = $record->id;
            }

            switch ($votetype) {
                case VOTE_TYPE_AV:
                    $mform->addElement('select', "option-$record->qid-$record->id", $record->optionname, $optionarray, ['class' => 'option']);
                    break;
                default:
                    $optionarray[] = $mform->createElement('radio', "question$record->qid", '',
                        $record->optionname, $record->id, ['class' => 'option']);
                    break;
            }
        }
        switch ($votetype) {
            case VOTE_TYPE_AV:
                break;
            default:
                if ($questionid !== null) {
                    $mform->addGroup($optionarray, "group_question$questionid", '',
                        [\html_writer::empty_tag('br')], false);

                    $mform->setDefault("question$questionid", $firstoption);
                }
                break;
        }

        if ($questionid === null) {
            // No questions to be displayed, we should tell the user.
            $message = $OUTPUT->notification(get_string('noquestions', 'mod_vote'), 'notifymessage');
            $mform->addElement('html', $message);
        }

        $rs->close();

        $this->add_action_buttons();
    }

    /**
     * Validate the form.
     *
     * @param array $data
     * @param array $files
     * @return array Error messages
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        // Validate the form in AV mode.
        // One or more options must be ranked.
        // No option can have a duplicate rank.
        if ($data['vt'] == VOTE_TYPE_AV) { // Validate AV voting form.
            $qid = null;
            $oid = null;
            $validator = new validate_rank();
            $allzero = false;
            foreach ($data as $key => $value) {
                $explodedkey = explode('-', $key);
                // All the data we need to validate is in the form option-<questionid>-<optionid>.
                if ($explodedkey[0] == 'option') {
                    if ($explodedkey[1] != $qid) {
                        if ($allzero) {
                            $errors["option-$qid-$oid"] = get_string('no_choice', 'mod_vote');
                        }

                        if (!$validator->validate()) {
                            $errors["option-$qid-$oid"] = get_string('invalid_choice', 'mod_vote');
                        }

                        $qid = $explodedkey[1];
                        $allzero = true; // Reset this value.
                        $validator = new validate_rank(); // Reset this variable.
                    }
                    $oid = $explodedkey[2];

                    if (!$validator->add($value, $oid)) { // Check if the rank has already been selected.
                        $errors["option-$qid-$oid"] = get_string('duplicate', 'mod_vote');
                    }

                    if ($value) { // The option is not 0.
                        $allzero = false;
                    }
                }
            }
            if ($allzero) {
                $errors["option-$qid-$oid"] = get_string('no_choice', 'mod_vote');
            }

            if (!$validator->validate()) {
                $errors["option-$qid-$oid"] = get_string('invalid_choice', 'mod_vote');
            }
        }
        return $errors;
    }
}
