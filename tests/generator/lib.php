<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * mod_vote data generator
 *
 * @package    mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_vote_generator extends testing_module_generator {

    /** @var int Count of questions created, this value is used to ensure that the default name of questions are unique. */
    protected $questioncount = 0;

    /** @var int count of option. */
    protected $optioncount = 0;

    /**
     * To be called from data reset code only,
     * do not use in tests.
     *
     * @return void
     */
    public function reset() {
        $this->questioncount = 0;
        $this->optioncount = 0;

        parent::reset();
    }

    /**
     * Creates an instance of the module for testing purposes.
     *
     * Module type will be taken from the class name. Each module type may overwrite
     * this function to add other default values used by it.
     *
     * @param array|stdClass $record data for module being generated. Requires 'course' key
     *     (an id or the full object). Also can have any fields from add module form.
     * @param null|array $options general options for course module. Since 2.6 it is
     *     possible to omit this argument by merging options into $record
     * @return stdClass record from module-defined table with additional field
     *     cmid (corresponding id in course_modules table)
     */
    public function create_instance($record = null, array $options = null) {
        $record = (object)(array)$record;

        require_once(dirname(__DIR__, 2).'/lib.php');

        if (!isset($record->votetype)) {
            $record->votetype = VOTE_TYPE_POLL;
        }
        if (!isset($record->closedate)) {
            $record->closedate = time() + 86400; // 24 hours after now.
        }
        if (!isset($record->votestate)) {
            $record->votestate = VOTE_STATE_ACTIVE;
        }
        if (!isset($record->completionvoted)) {
            $record->completionvoted = 0;
        }

        return parent::create_instance($record, (array)$options);
    }

    /**
     * Creates a question on a vote activity along with the options passed, then return the records.
     *
     * @param stdClass $vote - Record for the vote that the questoion should be attached to.
     * @param array|stdClass $question - Information about the question that should be created.
     * @param array $options - An array of information about the options that should be created
     *                         for the questionin the form of either an array or stdClass.
     * @return stdClass - The question record, with an additional propery (options) that
     *                    contains an array of the options that were created by this method.
     * @throws coding_exception
     */
    public function create_question(stdClass $vote, $question, array $options) {
        global $DB;

        // Do a basic check to see if a vote record was passed.
        if (!isset($vote->id) || !isset($vote->votetype)) {
            throw new coding_exception('A valid vote record must be passed to mod_vote_generator::create_question $vote');
        }

        // Create the question.
        $this->questioncount++;

        $question = (array)$question;

        $question['voteid'] = $vote->id;

        if (!isset($question['question'])) {
            $question['question'] = 'Question '.$this->questioncount;
        }

        if (!isset($question['sortorder'])) {
            $question['sortorder'] = 1;
        }

        $question = (object)$question;

        $question->id = $DB->insert_record('vote_question', $question);

        // Create the options, and add them into the question object we will return.
        $question->options = [];

        foreach ($options as $option) {
            $question->options[] = $this->create_option($question, $option);
        }

        return $question;
    }

    /**
     * Create an option for a question on a vote activity and return the record that is created.
     *
     * @param stdClass $question - Question that the option should be created for.
     * @param array|stdClass $option - Information that should be used to create the option.
     * @return stdClass - The option record.
     * @throws coding_exception
     */
    public function create_option(stdClass $question, $option) {
        global $DB;

        // Check a valid question record was passed.
        if (!isset($question->id) || !isset($question->voteid)) {
            throw new coding_exception('A valid question record must be passed to mod_vote_generator::create_option $question');
        }

        // Create the option.
        $this->optioncount++;

        $option = (array)$option;

        $option['voteid'] = $question->voteid;
        $option['questionid'] = $question->id;

        if (!isset($option['optionname'])) {
            $option['optionname'] = 'Option '.$this->optioncount;
        }

        if (!isset($option['sortorder'])) {
            $option['sortorder'] = 1;
        }

        $option = (object)$option;

        $option->id = $DB->insert_record('vote_options', $option);

        return $option;
    }

    /**
     * Enters the votes a user makes on a question into the database.
     *
     * @param stdClass $user - The database record for the user who is voting.
     * @param stdClass $question - The question that the votes are for.
     * @param array $options - The options the user has voted for, the order in the array is considered the ranking.
     * @return void
     * @throws coding_exception
     */
    public function create_votes(stdClass $user, stdClass $question, array $options) {
        // Basic check that a valid user was passed.
        if (!isset($user->id)) {
            throw new coding_exception('A valid user record must be passed to mod_vote_generator::create_votes $user');
        }

        // Basic check a valid question was passed.
        if (!isset($question->id) || !isset($question->voteid)) {
            throw new coding_exception('A valid question record must be passed to mod_vote_generator::create_votes $question');
        }

        if (empty($options)) {
            throw new coding_exception('At least one option must be passed to mod_vote_generator::create_votes $options');
        }

        // Create the voting records.
        $rank = 1;
        foreach ($options as $option) {
            if (!isset($option->id) || !isset($option->questionid) || !isset($option->voteid)) {
                throw new coding_exception('Valid option records must be passed to mod_vote_generator::create_votes $options');
            }

            if ($option->questionid !== $question->id || $option->voteid !== $question->voteid) {
                throw new coding_exception('The option records must be for the correct question mod_vote_generator::create_votes $options');
            }

            $this->create_vote($user, $option, $rank++);
        }
    }

    /**
     * Create a record that a user ranked an option.
     *
     * @param stdClass $user - The database record for the user who the vote is cast by.
     * @param stdClass $option - The option that the user voted for.
     * @param int $rank - The rank that the user voted for the option with (1 is the highest)
     * @return void
     */
    public function create_vote(stdClass $user, stdClass $option, $rank) {
        global $DB;

        if ($rank < 1) {
            throw new coding_exception('A vote may not have a rank below 1 mod_vote_generator::create_vote $rank');
        }

        $vote = new stdClass();
        $vote->voteid = $option->voteid;
        $vote->optionid = $option->id;
        $vote->userid = $user->id;
        $vote->vote = $rank;

        $DB->insert_record('vote_votes', $vote);
    }
}
