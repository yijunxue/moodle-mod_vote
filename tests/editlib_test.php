<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

use coding_exception;
use stdClass;

/**
 * Tests the vote activities mod_vote_cachelib.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 * @group uon
 */
final class editlib_test extends \advanced_testcase {
    /**
     * Tests that \mod_vote\editlib is able to add a question correctly.
     *
     * @covers \mod_vote\editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion(): void {
        global $DB;
        $this->resetAfterTest(true);

        // Test no questions exist.
        $this->assertEquals(0, $DB->count_records('vote_question'));

        // Add a question.
        $question = new stdClass();
        $question->voteid = 10;
        $question->question = 'Test Question';
        $question->sortorder = 1;
        \mod_vote\editlib::process_submittedquestion($question);

        // Test it was created correctly.
        $this->assertEquals(1, $DB->count_records('vote_question'));
        $questionrecord = $DB->get_record('vote_question', ['voteid' => $question->voteid]);
        $this->assertEquals($question->voteid, $questionrecord->voteid);
        $this->assertEquals($question->question, $questionrecord->question);
        $this->assertEquals($question->sortorder, $questionrecord->sortorder);

        $question2 = new stdClass();
        $question2->voteid = 11;
        $question2->question = 'Some question';
        $question2->sortorder = 5;
        \mod_vote\editlib::process_submittedquestion($question2);

        $this->assertEquals(2, $DB->count_records('vote_question'));
        $questionrecord2 = $DB->get_record('vote_question', ['voteid' => $question2->voteid]);
        $this->assertEquals($question2->voteid, $questionrecord2->voteid);
        $this->assertEquals($question2->question, $questionrecord2->question);
        $this->assertEquals($question2->sortorder, $questionrecord2->sortorder);

        // Test that the function updates correctly.
        $question3 = new stdClass();
        $question3->q = $questionrecord->id; // q should be passed by the form if it edited a question.
        $question3->voteid = 10;
        $question3->question = 'The question text has been changed';
        $question3->sortorder = 2;
        \mod_vote\editlib::process_submittedquestion($question3);

        $this->assertEquals(2, $DB->count_records('vote_question'));
        $questionrecord3 = $DB->get_record('vote_question', ['voteid' => $question3->voteid]);
        $this->assertEquals($question3->voteid, $questionrecord3->voteid);
        $this->assertEquals($question3->question, $questionrecord3->question);
        $this->assertEquals($question3->sortorder, $questionrecord3->sortorder);

        // Check that the other question was not modified.
        $questionrecord4 = $DB->get_record('vote_question', ['voteid' => $question2->voteid]);
        $this->assertEquals($questionrecord2, $questionrecord4);

        // Test adding a really long question string.
        // Test that the function updates correctly.
        $question4 = new stdClass();
        $question4->q = $questionrecord->id; // q should be passed by the form if it edited a question.
        $question4->voteid = 10;
        // 300 characters is larger than the database field.
        $question4->question = '1234566789012345667890123456678901234566789012345667890123456678901234566789012345667890123'
                . '456678901234566789012345667890123456678901234566789012345667890123456678901234566789012345667890123456678'
                . '901234566789012345667890123456678901234566789012345667890123456678901234566789012345667890123456678901234'
                . '56678901234566789012345667890123456678901234566789012345667890';
        $question4->sortorder = 2;
        \mod_vote\editlib::process_submittedquestion($question4);

        $this->assertEquals(2, $DB->count_records('vote_question'));
        $questionrecord5 = $DB->get_record('vote_question', ['voteid' => $question4->voteid]);
        $this->assertEquals($question4->voteid, $questionrecord5->voteid);
        $this->assertEquals(substr($question4->question, 0, 255), $questionrecord5->question);
        $this->assertEquals($question4->sortorder, $questionrecord5->sortorder);

        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that \mod_vote\editlib fails correctly when a voteid is not passed.
     *
     * @covers \mod_vote\editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion_no_voteid(): void {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $question = new stdClass();
        $question->question = 'Test Question';
        $question->sortorder = 1;

        $this->expectException(coding_exception::class);
        $this->expectExceptionMessage('$question->voteid was not set in mod_vote_editlib::process_submittedquestion');
        editlib::process_submittedquestion($question);
    }

    /**
     * Tests that \mod_vote\editlib fails correctly when a question is not passed.
     *
     * @covers \mod_vote\editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion_no_question(): void {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $question = new stdClass();
        $question->voteid = 10;
        $question->sortorder = 1;

        $this->expectException(coding_exception::class);
        $this->expectExceptionMessage('$question->question was not set in mod_vote_editlib::process_submittedquestion');
        editlib::process_submittedquestion($question);
    }

    /**
     * Tests that \mod_vote\editlib fails correctly when a sortorder is not passed.
     *
     * @covers \mod_vote\editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion_no_sortorder(): void {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $question = new stdClass();
        $question->voteid = 10;
        $question->question = 'Test Question';

        $this->expectException(coding_exception::class);
        $this->expectExceptionMessage('$question->sortorder was not set in mod_vote_editlib::process_submittedquestion');
        editlib::process_submittedquestion($question);
    }

    /**
     * Tests that \mod_vote\editlib is able to add a options correctly.
     *
     * @covers \mod_vote\editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption(): void {
        global $DB;
        $this->resetAfterTest(true);

        // Test no options exist.
        $this->assertEquals(0, $DB->count_records('vote_options'));

        // Add a question.
        $option = new stdClass();
        $option->voteid = 10;
        $option->q = 13; // The question's id.
        $option->optionname = 'Test Option';
        $option->sortorder = 1;
        editlib::process_submittedoption($option);

        // Test it was created correctly.
        $this->assertEquals(1, $DB->count_records('vote_options'));
        $optionrecord = $DB->get_record('vote_options', ['questionid' => $option->q]);
        $this->assertEquals($option->voteid, $optionrecord->voteid);
        $this->assertEquals($option->q, $optionrecord->questionid);
        $this->assertEquals($option->optionname, $optionrecord->optionname);
        $this->assertEquals($option->sortorder, $optionrecord->sortorder);

        // Create a second option on the same vote.
        $option2 = new stdClass();
        $option2->voteid = 10;
        $option2->q = 14;
        $option2->optionname = 'Another test Option';
        $option2->sortorder = 2;
        editlib::process_submittedoption($option2);

        // Test it was created correctly.
        $this->assertEquals(2, $DB->count_records('vote_options'));
        $optionrecord2 = $DB->get_record('vote_options', ['questionid' => $option2->q]);
        $this->assertEquals($option2->voteid, $optionrecord2->voteid);
        $this->assertEquals($option2->q, $optionrecord2->questionid);
        $this->assertEquals($option2->optionname, $optionrecord2->optionname);
        $this->assertEquals($option2->sortorder, $optionrecord2->sortorder);

        // Test updating an option.
        $option3 = new stdClass();
        $option3->o = $optionrecord->id;
        $option3->voteid = 10;
        $option3->q = 13;
        $option3->optionname = 'Changed Test Option';
        $option3->sortorder = 6;
        editlib::process_submittedoption($option3);

        // Test it was created correctly.
        $this->assertEquals(2, $DB->count_records('vote_options'));
        $optionrecord3 = $DB->get_record('vote_options', ['questionid' => $option->q]);
        $this->assertEquals($option3->voteid, $optionrecord3->voteid);
        $this->assertEquals($option3->q, $optionrecord3->questionid);
        $this->assertEquals($option3->optionname, $optionrecord3->optionname);
        $this->assertEquals($option3->sortorder, $optionrecord3->sortorder);

        // Check that the other option has not changed.
        $optionrecord4 = $DB->get_record('vote_options', ['questionid' => $option2->q]);
        $this->assertEquals($optionrecord2, $optionrecord4);

        // Test a long option name.
        $option4 = new stdClass();
        $option4->o = $optionrecord->id;
        $option4->voteid = 10;
        $option4->q = 13;
        $option4->optionname = 'Changed Test Option';
        $option4->sortorder = 6;
        editlib::process_submittedoption($option4);

        // Test it was created correctly.
        $this->assertEquals(2, $DB->count_records('vote_options'));
        $optionrecord5 = $DB->get_record('vote_options', ['questionid' => $option->q]);
        $this->assertEquals($option4->voteid, $optionrecord5->voteid);
        $this->assertEquals($option4->q, $optionrecord5->questionid);
        $this->assertEquals(substr($option4->optionname, 0, 255), $optionrecord5->optionname);
        $this->assertEquals($option4->sortorder, $optionrecord5->sortorder);

        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that \mod_vote\editlib::process_submittedoption fails correctly when a voteid is not passed.
     *
     * @covers \mod_vote\editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_voteid(): void {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->q = 13; // The question's id.
        $option->optionname = 'Test Option';
        $option->sortorder = 1;

        $this->expectException(coding_exception::class);
        $this->expectExceptionMessage('$option->voteid was not set in mod_vote_editlib::process_submittedoption');
        editlib::process_submittedoption($option);
    }

    /**
     * Tests that \mod_vote\editlib::process_submittedoption fails correctly when the question id is not passed.
     *
     * @covers \mod_vote\editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_questionid(): void {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->voteid = 10;
        $option->optionname = 'Test Option';
        $option->sortorder = 1;

        $this->expectException(coding_exception::class);
        $this->expectExceptionMessage('The question id ($option->q) was not set in mod_vote_editlib::process_submittedoption');
        editlib::process_submittedoption($option);
    }

    /**
     * Tests that \mod_vote\editlib::process_submittedoption fails correctly when a optionname is not passed.
     *
     * @covers \mod_vote\editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_optionname(): void {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->voteid = 10;
        $option->q = 13; // The question's id.
        $option->sortorder = 1;

        $this->expectException(coding_exception::class);
        $this->expectExceptionMessage('$option->optionname was not set in mod_vote_editlib::process_submittedoption');
        editlib::process_submittedoption($option);
    }

    /**
     * Tests that \mod_vote\editlib::process_submittedoption fails correctly when a sortorder is not passed.
     *
     * @covers \mod_vote\editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_sortorder(): void {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->voteid = 10;
        $option->q = 13; // The question's id.
        $option->optionname = 'Test Option';

        $this->expectException(coding_exception::class);
        $this->expectExceptionMessage('$option->sortorder was not set in mod_vote_editlib::process_submittedoption');
        editlib::process_submittedoption($option);
    }

    /**
     * Tests that \mod_vote\editlib deletes correctly.
     *
     * @covers \mod_vote\editlib::delete_option
     * @covers \mod_vote\editlib::delete_question
     * @group mod_vote
     * @group uon
     */
    public function test_delete(): void {
        global $DB;
        $this->resetAfterTest(true);

        require_once(dirname(__DIR__).'/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        // Create two vote activities and generate their cache.
        $user0 = self::getDataGenerator()->create_user();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $user4 = self::getDataGenerator()->create_user();

        // Create a course and add a vote activity to it.
        $course0 = self::getDataGenerator()->create_course();
        $vote0 = $votegenerator->create_instance(['course' => $course0->id, 'votetype' => VOTE_TYPE_POLL]);
        $question0 = $votegenerator->create_question(
                $vote0,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                    ['optionname' => 'Third option'],
                    ['optionname' => 'Forth option'],
                ]);

        // Enrol some users onto the course and get them to vote in the poll.
        self::getDataGenerator()->enrol_user($user0->id, $course0->id);
        self::getDataGenerator()->enrol_user($user1->id, $course0->id);
        self::getDataGenerator()->enrol_user($user2->id, $course0->id);
        self::getDataGenerator()->enrol_user($user3->id, $course0->id);
        self::getDataGenerator()->enrol_user($user4->id, $course0->id);

        $votegenerator->create_votes($user0, $question0, [$question0->options[2]]);
        $votegenerator->create_votes($user1, $question0, [$question0->options[0]]);
        $votegenerator->create_votes($user2, $question0, [$question0->options[0]]);
        $votegenerator->create_votes($user3, $question0, [$question0->options[1]]);
        $votegenerator->create_votes($user4, $question0, [$question0->options[3]]);

        // Get the cached results.
        $results = cachelib::get_cached_results(new \mod_vote\vote($vote0->id));

        $vote1 = $votegenerator->create_instance(['course' => $course0->id, 'votetype' => VOTE_TYPE_AV]);
        $question1 = $votegenerator->create_question(
                $vote1,
                ['question' => 'Test question 2'],
                [
                    ['optionname' => 'Option 1'],
                    ['optionname' => 'Option 4'],
                    ['optionname' => 'Option 3'],
                    ['optionname' => 'Option 2'],
                ]);
        $question2 = $votegenerator->create_question(
                $vote1,
                ['question' => 'Test question 3'],
                [
                    ['optionname' => 'Option 1'],
                    ['optionname' => 'Option 4'],
                    ['optionname' => 'Option 3'],
                    ['optionname' => 'Option 2'],
                ]);

        $votegenerator->create_votes($user0, $question1, [$question1->options[0], $question1->options[2]]);
        $votegenerator->create_votes($user1, $question1, [$question1->options[1]]);
        $votegenerator->create_votes($user2, $question1, [$question1->options[2]]);
        $votegenerator->create_votes($user3, $question1, [$question1->options[3], $question1->options[2]]);
        $votegenerator->create_votes($user4, $question1, [$question1->options[1]]);

        $votegenerator->create_votes($user1, $question2, [$question2->options[0], $question2->options[2]]);
        $votegenerator->create_votes($user0, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);
        $votegenerator->create_votes($user2, $question2, [$question2->options[2], $question2->options[3]]);
        $votegenerator->create_votes($user4, $question2, [$question2->options[3], $question2->options[2]]);
        $votegenerator->create_votes($user3, $question2, [$question2->options[1], $question2->options[2]]);

        // Get the cached results.
        $results2 = cachelib::get_cached_results(new \mod_vote\vote($vote1->id));

        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(3, $DB->count_records('vote_question'));
        $this->assertEquals(12, $DB->count_records('vote_options'));
        $this->assertEquals(23, $DB->count_records('vote_votes'));
        $this->assertEquals(22, $DB->count_records('vote_result_cache'));

        // Setup completed, so start the tests.

        editlib::delete_option($question2->options[0]->id);
        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(3, $DB->count_records('vote_question'));
        $this->assertEquals(11, $DB->count_records('vote_options'));
        $this->assertEquals(21, $DB->count_records('vote_votes'));
        $this->assertEquals(21, $DB->count_records('vote_result_cache'));
        $this->assertEquals(0, $DB->count_records('vote_options', ['id' => $question2->options[0]->id]));
        $this->assertEquals(0, $DB->count_records('vote_votes', ['optionid' => $question2->options[0]->id]));
        $this->assertEquals(0, $DB->count_records('vote_result_cache', ['optionid' => $question2->options[0]->id]));

        editlib::delete_question($question1->id);
        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(2, $DB->count_records('vote_question'));
        $this->assertEquals(7, $DB->count_records('vote_options'));
        $this->assertEquals(14, $DB->count_records('vote_votes'));
        $this->assertEquals(12, $DB->count_records('vote_result_cache'));
        $this->assertEquals(0, $DB->count_records('vote_question', ['id' => $question1->id]));
        $this->assertEquals(0, $DB->count_records('vote_options', ['questionid' => $question1->id]));

        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that \mod_vote\editlib can make a vote active.
     *
     * @covers \mod_vote\editlib::make_active
     * @group mod_vote
     * @group uon
     */
    public function test_make_active(): void {
        global $DB;
        $this->resetAfterTest(true);

        require_once(dirname(__DIR__).'/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        // Test setup.
        $course0 = self::getDataGenerator()->create_course();
        $vote0 = $votegenerator->create_instance(['course' => $course0->id, 'votestate' => VOTE_STATE_EDITING]);
        $vote1 = $votegenerator->create_instance(['course' => $course0->id, 'votestate' => VOTE_STATE_EDITING]);

        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(0, $DB->count_records('vote', ['votestate' => VOTE_STATE_ACTIVE]));

        // Starts the test.
        editlib::make_active($vote0->id);
        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(1, $DB->count_records('vote', ['votestate' => VOTE_STATE_ACTIVE]));
        $this->assertEquals(1, $DB->count_records('vote', ['votestate' => VOTE_STATE_ACTIVE, 'id' => $vote0->id]));
    }
}
