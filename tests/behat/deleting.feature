@mod @mod_vote @uon
Feature: Deleting questions and options.
    In order to manage voting options
    As a teacher
    I need to be able to delete questions and options

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
        # Note: The type of vote makes no difference to this test.
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 0 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
            | vote1 | Question to be deleted |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | Question to be deleted | to be deleted 1 |
            | vote1 | Question to be deleted | to be deleted 2 |

    Scenario: Delete an option
        Given I am on the "Poll test" "mod_vote > View" page logged in as "teacher1"
        When I delete the "to be deleted 1" option from "Question to be deleted" in the vote
        Then I should not see "to be deleted 1"
        But I should see "to be deleted 2"
        And I should see "Green"
        And I should see "Blue"

    Scenario: Delete a question
        Given I am on the "Poll test" "mod_vote > View" page logged in as "teacher1"
        When I delete the "Question to be deleted" question in the vote
        Then I should not see "Question to be deleted"
        And I should not see "to be deleted 1"
        And I should not see "to be deleted 2"
        But I should see "What colour is the sky?"
        And I should see "Green"
        And I should see "Blue"
