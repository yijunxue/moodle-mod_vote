<?php
// This file is part of mod_vote
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Steps definitions related with the vote activity.
 *
 * @package    mod_vote
 * @category   test
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2016 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// NOTE: no MOODLE_INTERNAL test here, this file may be required by behat before including /config.php.

require_once(__DIR__ . '/../../../../lib/behat/behat_base.php');

use Behat\Gherkin\Node\TableNode,
    Behat\Behat\Exception\PendingException;

/**
 * Vote-related steps definitions.
 *
 * @package    mod_vote
 * @category   test
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2016 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class behat_mod_vote extends behat_base {
    /**
     * Gets the id of the html element that is used to vote for an option when the alternative voting method is used.
     *
     * @param int $questionid The id of the question the option should be for
     * @param string $option the name of the option to vote for
     * @return string
     */
    protected function get_av_voting_field($questionid, $option) {
        global $DB;
        $optionrecord = $DB->get_record('vote_options', ['questionid' => $questionid, 'optionname' => $option], 'id', MUST_EXIST);
        return "id_option-{$questionid}-{$optionrecord->id}";
    }

    /**
     * Gets the id of the html element that is used to vote for an option.
     *
     * @param int $questionid The id of the question the option should be for
     * @param string $option the name of the option to vote for
     * @return string
     */
    protected function get_vote_voting_field($questionid, $option) {
        global $DB;
        $optionrecord = $DB->get_record('vote_options', ['questionid' => $questionid, 'optionname' => $option], 'id', MUST_EXIST);
        return "id_question{$questionid}_{$optionrecord->id}";
    }

    /**
     * Gets the steps required for a user to cast a vote for a specific option.
     *
     * @param array $vote an array of values passed to the calling step representing a vote record.
     * @param int $voteid the id of the vote record the question is for.
     * @param int $method the type of vote
     * @throws Exception
     */
    protected function get_voting_step($vote, $voteid, $method) {
        global $DB;
        if (!isset($vote['question'])) {
            throw new Exception('You must define the "question" that is being voted on');
        }
        if (!isset($vote['vote'])) {
            throw new Exception('You must define the "vote" option the user is voting for');
        }
        // Get the question record.
        $question = $DB->get_record('vote_question', ['voteid' => $voteid, 'question' => $vote['question']], '*', MUST_EXIST);
        switch ($method) {
            case 3:
                // Alternative vote.
                if (!isset($vote['rank'])) {
                    throw new Exception('You must define the "rank" the user set for the option');
                }
                $option = $vote['rank'];
                $fieldname = $this->get_av_voting_field($question->id, $vote['vote']);
                break;
            default:
                // All other vote types.
                $option = $vote['vote'];
                $fieldname = $this->get_vote_voting_field($question->id, $option);
                break;
        }
        $this->execute('behat_forms::i_set_the_field_to', [$fieldname, $this->escape($option)]);
    }

    /**
     * Add a question to a vote that the user is already in.
     *
     * @Given /^I add a question to the vote and I fill the form with:$/
     * @param TableNode $options The form fields that should be modified.
     */
    public function i_add_a_question_to_the_vote_and_i_fill_the_form_with(TableNode $options) {
        $addquestionstring = get_string('add_question', 'mod_vote');
        $this->execute('behat_general::click_link', $this->escape($addquestionstring));
        foreach ($options->getRowsHash() as $option => $value) {
            $this->execute('behat_forms::i_set_the_field_to', [$option, $this->escape($value)]);
        }
        $this->execute('behat_forms::press_button', get_string('savechanges', 'moodle'));
    }

    /**
     * Add an option to a specific question filling in the form. Requires that you are already in a Vote activity.
     *
     * @Given /^I add an option to "(?P<question_string>(?:[^"]|\\")*)" and I fill the form with:$/
     * @param string $question The name of the queastion the option should be added to
     * @param TableNode $options The form fields that should be modified.
     */
    public function i_add_an_option_to_question_and_i_fill_the_form_with($question, TableNode $options) {
        $addoptionstring = get_string('add_option', 'mod_vote');
        $questionselector = $this->question_edit_xpath($question);
        $this->execute("behat_general::i_click_on_in_the", [$addoptionstring, 'link', $this->escape($questionselector), "xpath_element"]);

        foreach ($options->getRowsHash() as $option => $value) {
            $this->execute('behat_forms::i_set_the_field_to', [$option, $this->escape($value)]);
        }
        $this->execute('behat_forms::press_button', get_string('savechanges', 'moodle'));
    }

    /**
     * Delete an option from a vote. The user must be a vote editor, viewing a vote that is not active.
     *
     * @When /^I delete the "(?P<option_string>(?:[^"]|\\")*)" option from "(?P<question_string>(?:[^"]|\\")*)" in the vote$/
     * @param string $option The name of the option to be deleted
     * @param string $question The name of the question the option is in
     */
    public function i_delete_the_option_from_in_the_vote($option, $question) {
        $deletestring = get_string('delete');
        $optiondeleteselector = $this->option_edit_xpath($option) . $this->menu_xpath($deletestring);
        $questionselector = $this->question_edit_xpath($question);
        $this->execute("behat_general::i_click_on_in_the", [$optiondeleteselector, "xpath_element", $questionselector, "xpath_element"]);
    }

    /**
     * Delete a question from a vote. The user must be a vote editor, viewing a vote that is not active.
     *
     * @When /^I delete the "(?P<question_string>(?:[^"]|\\")*)" question in the vote$/
     * @param string $question The name of the question to be deleted.
     */
    public function i_delete_the_question_in_the_vote($question) {
        $deletestring = get_string('delete');
        $questiondeleteselector = $this->menu_xpath($deletestring);
        $questionselector = $this->question_edit_xpath($question);
        $this->execute("behat_general::i_click_on_in_the", [$questiondeleteselector, "xpath_element", $questionselector, "xpath_element"]);
    }

    /**
     * Edit an option in a vote to have the new values.
     *
     * @When /^I edit the "(?P<option_string>(?:[^"]|\\")*)" option in "(?P<question_string>(?:[^"]|\\")*)" and I fill the form with:$/
     * @param string $option The name of the option
     * @param string $question The name of the question the option is part of
     * @param TableNode $values the new values for the option
     */
    public function i_edit_the_option_in_and_i_fill_the_form_with($option, $question, TableNode $values) {
        $editstring = get_string('edit');
        $questionselector = $this->question_edit_xpath($question);
        $editoptionselect = $this->option_edit_xpath($option) . $this->menu_xpath($editstring);
        $this->execute("behat_general::i_click_on_in_the", [$editoptionselect, "xpath_element", $questionselector, "xpath_element"]);

        foreach ($values->getRowsHash() as $field => $value) {
            $this->execute('behat_forms::i_set_the_field_to', [$field , $this->escape($value)]);
        }
        $this->execute('behat_forms::press_button', get_string('savechanges', 'moodle'));
    }

    /**
     * Edit a question in a vote to the new values.
     *
     * @When /^I edit the "(?P<question_string>(?:[^"]|\\")*)" question and I fill the form with:$/
     * @param string $question The name of the question
     * @param TableNode $values The new form information
     */
    public function i_edit_the_question_and_i_fill_the_form_with($question, TableNode $values) {
        $editstring = get_string('edit');
        $questionselector = $this->question_edit_xpath($question);
        $editselect = $this->menu_xpath($editstring);
        $this->execute("behat_general::i_click_on_in_the", [$editselect, "xpath_element", $questionselector, "xpath_element"]);
        foreach ($values->getRowsHash() as $field => $value) {
            $this->execute('behat_forms::i_set_the_field_to', [$field, $this->escape($value)]);
        }
        $this->execute('behat_forms::press_button', get_string('savechanges', 'moodle'));
    }

    /**
     * Fills in the voting form for a user. Must be in the vote activity already.
     *
     * @When /^I fill in the "(?P<vote_string>(?:[^"]|\\")*)" vote with:$/
     *
     * @param string $vote The name of the vote
     * @param TableNode $votes Information about how the user will vote.
     */
    public function i_fill_in_the_vote_with($vote, TableNode $votes) {
        global $DB;
        // Get the vote record.
        $voterecord = $DB->get_record('vote', ['name' => $vote], '*', MUST_EXIST);
        foreach ($votes->getHash() as $votecast) {
            $this->get_voting_step($votecast, $voterecord->id, $voterecord->votetype);
        }
        $this->execute('behat_forms::press_button', get_string('savechanges', 'moodle'));
    }

    /**
     * Make a vote active. The user must be a vote editor, viewing a vote that is not active.
     *
     * @When /^I make the vote active$/
     */
    public function i_make_the_vote_active() {
        $this->execute('behat_general::click_link', $this->escape(get_string('activate', 'mod_vote')));
    }

    /**
     * Resets the vote cache of a vote activity, when it is displaying results.
     * Must be used from inside the activity you wish to reset.
     *
     * @When /^I reset the vote cache$/
     */
    public function i_reset_the_vote_cache() {
        $string = get_string('delete_cache', 'mod_vote');
        $this->execute("behat_general::click_link", $this->escape($string));
    }

    /**
     * Set the date in the vote creation form. Note the form works in increments of 5 minutes so this will b
     *
     * @Given /^I set the close date of the vote to be "(?P<time_int>(?:[^"]|\\")*)" "(?P<units_string>(?:[^"]|\\")*)" in the "(?P<direction_string>(?:[^"]|\\")*)" as "(?P<username_string>(?:[^"]|\\")*)"$/
     *
     * @param int $time The minimum amount of time that the date should be from now.
     * @param string $units The units of time to use (minutes, hours, days)
     * @param string $direction The direction the time should be set in (past, future)
     * @param string $user Username of a moodle user account.
     * @throws Exception
     */
    public function i_set_the_close_date_of_the_vote_to_be_time_units_in_the_direction($time, $units, $direction, $user) {
        global $DB;
        // Make the relative time.
        switch ($units) {
            case 'minutes':
                $multiplier = 60;
                break;
            case 'hours':
                $multiplier = 3600;
                break;
            case 'days':
                $multiplier = 86400;
                break;
            default:
                throw Exception('The second parameter must be one of the following: minutes, hours, days');
                break;
        }
        // Calculate the target timestamp.
        $modifiedtime = $time * $multiplier;
        if ($direction == 'future') {
            $timetoset = time() + $modifiedtime;
        } else if ($direction == 'past') {
            $timetoset = time() - $modifiedtime;
        } else {
            throw Exception('The third parameter must be one of the following: future, past');
        }
        // Get the user record.
        $user = $DB->get_record('user', ['username' => $user]);
        // Find the user's timezone.
        $datearray = usergetdate($timetoset, $user->timezone);
        // Set the minutes value to a value that is rounded to the next 5 minute interval.
        $minutes = $datearray['minutes'];
        if ($direction == 'future') {
            // Round up to the nearest 5 minutes.
            $setminutes = (ceil($minutes / 5) * 5);
        } else if ($direction == 'past') {
            // Round down to the nearest 5 minutes.
            $setminutes = (floor($minutes / 5) * 5);
        }
        // Set the values in the form.
        $this->execute('behat_forms::i_set_the_field_to', ["closedate[day]", $this->escape($datearray['mday'])]);
        $this->execute('behat_forms::i_set_the_field_to', ["closedate[month]", $this->escape($datearray['mon'])]);
        $this->execute('behat_forms::i_set_the_field_to', ["closedate[year]", $this->escape($datearray['year'])]);
        $this->execute('behat_forms::i_set_the_field_to', ["closedate[hour]", $this->escape($datearray['hours'])]);
        $this->execute('behat_forms::i_set_the_field_to', ["closedate[minute]", $this->escape($setminutes)]);
    }

    /**
     * Ensure that the questions exist in the vote and are in the order specified.
     *
     * @Then /^I should see questions in the following order:$/
     * @param TableNode $questions The names of the questions in the order that they should appear.
     */
    public function i_should_see_questions_in_the_following_order(TableNode $questions) {
        $return = [];
        $position = 1; // The CCS position selector for the question.
        foreach ($questions->getRows() as $question) {
            $questionselector1 = $this->question_at_position_edit_xpath($position);
            $questionselector2 = $this->question_at_position_vote_xpath($position);
            $questionselector = $questionselector1 . ' | ' . $questionselector2;
            $this->execute('behat_general::assert_element_contains_text', [$this->escape($question[0]), $questionselector, 'xpath_element']);
            $position++;
        }
        return $return;
    }

    /**
     * Checks that a question in the vote does not contain a specified option.
     *
     * @Given /^I should see the "(?P<question_string>(?:[^"]|\\")*)" question does not have "(?P<option_string>(?:[^"]|\\")*)" option$/
     * @param string $question The name of the question
     * @param string $option The name of the option
     */
    public function i_should_see_the_question_does_not_have_option($question, $option) {
        $questionselector = $this->question_edit_xpath($question);
        $this->execute('behat_general::assert_element_not_contains_text', [$this->escape($option), $questionselector, 'xpath_element']);
    }

    /**
     * Ensure that the specified question has the a set of options in the correct order.
     *
     * @Then /^I should see the "(?P<question_string>(?:[^"]|\\")*)" question has the following ordered options:$/
     * @param string $question Then name of a question
     * @param TableNode $options The names of the options that should be present for the question in the order they should appear.
     */
    public function i_should_see_the_question_has_the_following_ordered_options($question, TableNode $options) {
        $questionselector = $this->question_edit_xpath($question);
        $questionselector2 = $this->question_vote_xpath($question);
        $return = [];
        $position = 1; // The CCS position selector for the option.
        foreach ($options->getRows() as $option) {
            $optionselector1 = $questionselector . $this->option_at_position_edit_xpath($position);
            $optionselector2 = $questionselector2 . $this->option_at_position_vote_xpath($position);
            $optionselector3 = $questionselector2 . $this->option_at_position_vote_av_xpath($position);
            $optionselector = $optionselector1 . ' | ' . $optionselector2 . ' | ' . $optionselector3;
            $this->execute('behat_general::assert_element_contains_text', [$this->escape($option[0]), $optionselector, 'xpath_element']);
            $position++;
        }
        return $return;
    }

    /**
     * Get the xpath requires to select a menu item.
     *
     * @param string $option the text of the menu option.
     * @return string
     */
    protected function menu_xpath($option) {
        return "//a[.//*[@title='" . $option . "']]";
    }

    /**
     * Return the xpath required to find an option at a specific position in the ediing view.
     *
     * @param int $position The position the option is in the questions list of options (counts from 1)
     * @return string
     */
    protected function option_at_position_edit_xpath($position) {
        return "//*[contains(concat(' ', normalize-space(@class), ' '), ' mod_vote_option ')][" . $this->escape($position) . "]";
    }

    /**
     * Return the xpath required to find an option at a specific position in the voting view, for votes and polls.
     *
     * @param int $position The position the option is in the questions list of options (counts from 1)
     * @return string
     */
    protected function option_at_position_vote_xpath($position) {
        return "//*[contains(concat(' ', normalize-space(@class), ' '), ' felement ')]"
        . "//label[contains(concat(' ', normalize-space(@class), ' '), ' option ')][" . $this->escape($position) . "]";
    }

    /**
     * Return the xpath required to find an option at a specific position in the voting view, for alternative votes.
     *
     * @param int $position The position the option is in the questions list of options (counts from 1)
     * @return string
     */
    protected function option_at_position_vote_av_xpath($position) {
        return "//div//div[contains(concat(' ', normalize-space(@class), ' '), ' fitem ') and "
        . ".//select][" . $this->escape($position) . "]//label";
    }

    /**
     * Generates an xpath string to select the name of an option in the editing view.
     *
     * @param string $option The name of the option
     * @return string
     */
    protected function option_edit_xpath($option) {
        return "//*[contains(concat(' ', normalize-space(@class), ' '), ' mod_vote_option ') "
                . "and text()[contains(., '" . $this->escape($option) . "')]]";
    }

    /**
     * Return the xpath required to find a question at a specific position in the ediing view.
     *
     * @param int $position The position the option is in the questions list of options (counts from 1)
     * @return string
     */
    protected function question_at_position_edit_xpath($position) {
        return "//*[contains(concat(' ', normalize-space(@class), ' '), ' mod_vote_question ')][" . $this->escape($position) . "]";
    }

    /**
     * Return the xpath required to find a question at a specific position in the voting view.
     *
     * @param int $position The position the option is in the questions list of options (counts from 1)
     * @return string
     */
    protected function question_at_position_vote_xpath($position) {
        return "//fieldset[./legend][" . $this->escape($position) . "]";
    }

    /**
     * Generate the xpath required to find a question in editing mode.
     *
     * @param string $question
     * @return string
     */
    protected function question_edit_xpath($question) {
        return "//*[contains(concat(' ', normalize-space(@class), ' '), ' mod_vote_question ') "
                . "and .//h3[.//text()[contains(., '" . $this->escape($question) . "')]]]";
    }

    /**
     * Generate the xpath required to find a question in voting mode.
     *
     * @param string $question
     * @return string
     */
    protected function question_vote_xpath($question) {
        return "//fieldset[.//legend[.//text()[contains(., '" . $this->escape($question) . "')]]]";
    }

    /**
     * Create questions, options or votes
     *
     * Requires that the following are defined:
     * - vote: the idnumber of the vote activity
     * - question: the name of the question
     * - optionname: the name of the option (for options and votes only)
     * - user: the username that has votes (for votes only)
     *
     * Optional:
     * - sortorder: the weighting value for the question or activity
     * - rank: The rank of the vote (starts at 1)
     *
     * @Given /^the following vote "(?P<element_string>(?:[^"]|\\")*)" exist:$/
     *
     * @param string $type The type of data to be created
     * @param TableNode $elements The data to be created
     * @throws Exception
     * @throws PendingException when a type that has not been coded for is passed
     */
    public function the_following_vote_elements_exist($type, TableNode $elements) {
        global $DB;
        $votegenerator = testing_util::get_data_generator()->get_plugin_generator('mod_vote');
        foreach ($elements->getHash() as $element) {
            // Validate element fields are present.
            if (!isset($element['vote'])) {
                throw new Exception('vote must be specified');
            }
            if ($type !== 'questions' && !isset($element['question'])) {
                throw new Exception('question must be specified');
            }
            if ($type === 'votes' && !isset($element['optionname'])) {
                throw new Exception('optionname must be specified');
            }
            if ($type === 'votes' && !isset($element['user'])) {
                throw new Exception('user must be specified');
            }
            // Get the vote record.
            $voteid = $DB->get_field('course_modules', 'instance', ['idnumber' => $element['vote']], MUST_EXIST);
            $vote = $DB->get_record('vote', ['id' => $voteid], '*', MUST_EXIST);

            switch ($type) {
                case 'questions':
                    $votegenerator->create_question($vote, $element, []);
                    break;
                case 'options':
                    // Get question record.
                    $question = $DB->get_record('vote_question', ['voteid' => $vote->id, 'question' => $element['question']], '*', MUST_EXIST);
                    $votegenerator->create_option($question, $element);
                    break;
                case 'votes':
                    // Get the user, question and option.
                    $question = $DB->get_record('vote_question', ['voteid' => $vote->id, 'question' => $element['question']], '*', MUST_EXIST);
                    $option = $DB->get_record('vote_options', ['questionid' => $question->id, 'optionname' => $element['optionname']], '*', MUST_EXIST);
                    $user = $DB->get_record('user', ['username' => $element['user']], '*', MUST_EXIST);
                    // Set the passed rank, otherwise use a default value of 1.
                    $rank = (isset($element['rank'])) ? $element['rank'] : 1;
                    $votegenerator->create_vote($user, $option, $rank);
                    break;
                default:
                    throw new PendingException("Creation of vote $type has not been implemented.");
            }
        }
    }

    /**
     * Convert page names to URLs for steps.
     *
     * Recognised page names are:
     * | Page type     | Identifier meaning | description                                     |
     * | View          | Vote name          | The main vote activity page                     |
     *
     * @param string $type identifies which type of page this is, e.g. 'Attempt review'.
     * @param string $identifier identifies the particular page, e.g. 'Test quiz > student > Attempt 1'.
     * @return moodle_url the corresponding URL.
     * @throws Exception with a meaningful error message if the specified page cannot be found.
     */
    protected function resolve_page_instance_url(string $type, string $identifier): moodle_url {
        switch ($type) {
            case 'View':
                return $this->resolve_view_page_url($identifier);
            default:
                throw new Exception('Unrecognised signup sheet page type "' . $type . '."');
        }
    }

    /**
     * Gets the id of a vote from the name.
     *
     * @param string $name
     * @return int
     * @throws Exception
     */
    protected function resolve_vote_id_from_name(string $name): int {
        global $DB;
        $id = $DB->get_field('vote', 'id', ['name' => trim($name)]);
        if (!$id) {
            throw new Exception("The specified vote with name '$name' does not exist");
        }
        return $id;
    }

    /**
     * Generates the url for the main view page of a vote activity.
     *
     * @param string $identifier
     * @return \moodle_url
     * @throws \coding_exception
     * @throws \moodle_exception
     */
    protected function resolve_view_page_url(string $identifier): moodle_url {
        $id = $this->resolve_vote_id_from_name($identifier);
        $cm = get_coursemodule_from_instance('vote', $id);
        return new moodle_url('/mod/vote/view.php', ['id' => $cm->id]);
    }
}
