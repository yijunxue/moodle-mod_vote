@mod @mod_vote @uon
Feature: Vote secrecy
    While a vote is active to maintain secrecy
    As a student
    I should not be able to see the results.

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student1@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
        # votetype 2 is a vote.
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Vote test | Votes for cash | 2 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |

    Scenario: A student should not be able to see the results while voting is not closed.
        Given I am on the "Vote test" "mod_vote > View" page logged in as "student1"
        When I fill in the "Vote test" vote with:
            | question | vote |
            | What colour is the sky? | Blue |
        Then I should see "Thank you for voting"
        But I should not see "Blue (1 votes)"
        And I should not see "Green (0 votes)"
        # Ensure that leaving the vote and going back in does not let you see the results.
        And I am on the "Vote test" "mod_vote > View" page
        And I should see "Thank you for voting"
        But I should not see "Blue (1 votes)"
        And I should not see "Green (0 votes)"

    @app @javascript
    Scenario: Voting in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "Vote test" in the app
        And I select "Blue" near "What colour is the sky?" in the app
        And I press "Submit vote" in the app
        Then I should find "Thank you for voting, please come back after" in the app
        But I should not find "Blue (1 votes)" in the app
        And I should not find "Green (0 votes)" in the app
