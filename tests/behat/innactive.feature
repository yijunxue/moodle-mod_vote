@mod @mod_vote @uon
Feature: Inactive votes
    In order to allow votes to be edited safely
    As a student
    I should not be able to see the options.

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student1@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
        # votesate 0 is not active
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 0 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |

    Scenario: Inactive polls are not available to students
        When I am on the "Poll test" "mod_vote > View" page logged in as "student1"
        Then I should see "Voting is not open right now, please come back later."

    @app @javascript
    Scenario: Inactive polls are not available to students in the app
        Given I entered the course "Course 1" as "student1" in the app
        When I press "Poll test" in the app
        Then I should find "Voting is not open right now, please come back later." in the app
