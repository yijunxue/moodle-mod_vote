@mod @mod_vote @uon
Feature: Viewing alternative vote results
    When viewing a closed vote
    As a user
    I should see the correct results

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
            | student1 | Student | 1 | student1@example.com |
            | student2 | Student | 2 | student2@example.com |
            | student3 | Student | 3 | student3@example.com |
            | student4 | Student | 4 | student4@example.com |
            | student5 | Student | 5 | student4@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
            | student1 | C1 | student |
            | student2 | C1 | student |
            | student3 | C1 | student |
            | student4 | C1 | student |
            | student5 | C1 | student |
        # votetype 3 is an alternative vote, the close date is set to the earliest possible timestamp.
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate | closedate |
            | vote | C1 | vote1 | AV test | The cake is a lie! | 3 | 1 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | Which desert do you want? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | Which desert do you want? | Cake |
            | vote1 | Which desert do you want? | Lie |
            | vote1 | Which desert do you want? | Pie |

    Scenario: A user who voted can see the results
        Given the following vote "votes" exist:
            | vote | question | user | optionname | rank |
            | vote1 |  Which desert do you want? | student1 | Cake | 1 |
            | vote1 |  Which desert do you want? | student1 | Lie | 2 |
            | vote1 |  Which desert do you want? | student1 | Pie | 3 |
            | vote1 |  Which desert do you want? | student2 | Cake | 2 |
            | vote1 |  Which desert do you want? | student2 | Lie | 3 |
            | vote1 |  Which desert do you want? | student2 | Pie | 1 |
            | vote1 |  Which desert do you want? | student3 | Cake | 3 |
            | vote1 |  Which desert do you want? | student3 | Lie | 2 |
            | vote1 |  Which desert do you want? | student3 | Pie | 1 |
            | vote1 |  Which desert do you want? | student4 | Cake | 3 |
            | vote1 |  Which desert do you want? | student4 | Lie | 1 |
            | vote1 |  Which desert do you want? | student4 | Pie | 2 |
            | vote1 |  Which desert do you want? | student5 | Cake | 1 |
            | vote1 |  Which desert do you want? | student5 | Lie | 3 |
            | vote1 |  Which desert do you want? | student5 | Pie | 2 |
        And I am on the "AV test" "mod_vote > View" page logged in as "student1"
        Then I should see "Cake (2 votes)"
        And I should see "Lie (1 votes) Eliminated in round 1"
        And I should see "Pie (3 votes)"

    @app @javascript
    Scenario: A user who voted can see the results in the app
        Given the following vote "votes" exist:
            | vote | question | user | optionname | rank |
            | vote1 |  Which desert do you want? | student1 | Cake | 1 |
            | vote1 |  Which desert do you want? | student1 | Lie | 2 |
            | vote1 |  Which desert do you want? | student1 | Pie | 3 |
            | vote1 |  Which desert do you want? | student2 | Cake | 2 |
            | vote1 |  Which desert do you want? | student2 | Lie | 3 |
            | vote1 |  Which desert do you want? | student2 | Pie | 1 |
            | vote1 |  Which desert do you want? | student3 | Cake | 3 |
            | vote1 |  Which desert do you want? | student3 | Lie | 2 |
            | vote1 |  Which desert do you want? | student3 | Pie | 1 |
            | vote1 |  Which desert do you want? | student4 | Cake | 3 |
            | vote1 |  Which desert do you want? | student4 | Lie | 1 |
            | vote1 |  Which desert do you want? | student4 | Pie | 2 |
            | vote1 |  Which desert do you want? | student5 | Cake | 1 |
            | vote1 |  Which desert do you want? | student5 | Lie | 3 |
            | vote1 |  Which desert do you want? | student5 | Pie | 2 |
        And I entered the course "Course 1" as "student1" in the app
        When I press "AV test" in the app
        Then I should find "Cake (2 votes)" in the app
        And I should find "Lie (1 votes) Eliminated in round 1" in the app
        And I should find "Pie (3 votes)" in the app

    Scenario Outline: A user who did not vote can see the results
        Given the following vote "votes" exist:
            | vote | question | user | optionname | rank |
            | vote1 |  Which desert do you want? | student2 | Cake | 2 |
            | vote1 |  Which desert do you want? | student2 | Lie | 3 |
            | vote1 |  Which desert do you want? | student2 | Pie | 1 |
            | vote1 |  Which desert do you want? | student3 | Cake | 3 |
            | vote1 |  Which desert do you want? | student3 | Lie | 2 |
            | vote1 |  Which desert do you want? | student3 | Pie | 1 |
            | vote1 |  Which desert do you want? | student4 | Cake | 3 |
            | vote1 |  Which desert do you want? | student4 | Lie | 1 |
            | vote1 |  Which desert do you want? | student4 | Pie | 2 |
            | vote1 |  Which desert do you want? | student5 | Cake | 1 |
            | vote1 |  Which desert do you want? | student5 | Lie | 3 |
            | vote1 |  Which desert do you want? | student5 | Pie | 2 |
        And I am on the "AV test" "mod_vote > View" page logged in as "<user>"
        Then I should see "Cake (1 votes)"
        And I should see "Lie (1 votes)"
        And I should see "Pie (2 votes)"
        But I should not see "Eliminated in round 1"

        Examples:
            | user |
            | student1 |
            | teacher1 |
