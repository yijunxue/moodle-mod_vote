@mod @mod_vote @uon
Feature: Weighting options.
    In order to manage the display order of options
    As a teacher
    I need to be able to change the weighting of options

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 0 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | What colour is the sky? | Red |
            | vote1 | What colour is the sky? | Yellow |

    Scenario: Editing teacher sets weights on options
        Given I log in as "teacher1"
        And I am on "Course 1" course homepage
        And I follow "Poll test"
        Then I should see the "What colour is the sky?" question has the following ordered options:
            | Blue |
            | Green |
            | Red |
            | Yellow |
        When I edit the "Green" option in "What colour is the sky?" and I fill the form with:
            | Weighting | 4 |
        Then I should see the "What colour is the sky?" question has the following ordered options:
            | Blue |
            | Red |
            | Yellow |
            | Green |
        When I edit the "Blue" option in "What colour is the sky?" and I fill the form with:
            | Weighting | 2 |
        Then I should see the "What colour is the sky?" question has the following ordered options:
            | Red |
            | Yellow |
            | Blue |
            | Green |
