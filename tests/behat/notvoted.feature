@mod @mod_vote @uon
Feature: Student not voted
    In order to be able to vote
    As a student
    I should not see any results if I have not voted, unless the poll is closed

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
            | student1 | Student | 1 | student1@example.com |
            | student2 | Student | 2 | student2@example.com |
            | student3 | Student | 3 | student3@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
            | student1 | C1 | student |
            | student2 | C1 | student |
            | student3 | C1 | student |

    Scenario: Student should not see the results with out having voted.
        Given the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | What colour is the sky? | Red |
        And the following vote "votes" exist:
            | vote | question | user | optionname |
            | vote1 |  What colour is the sky? | student1 | Blue |
            | vote1 |  What colour is the sky? | student2 | Green |
        When I am on the "Poll test" "mod_vote > View" page logged in as "student3"
        Then I should not see "Blue (1 votes)"
        And I should not see "Green (1 votes)"
        And I should not see "Red (0 votes)"

    Scenario: Students should see results after a vote closes.
        Given the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate | closedate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 1 | ##yesterday## |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |
            | vote1 | What colour is the sky? | Red |
        And the following vote "votes" exist:
            | vote | question | user | optionname |
            | vote1 |  What colour is the sky? | student1 | Blue |
            | vote1 |  What colour is the sky? | student2 | Green |
        When I am on the "Poll test" "mod_vote > View" page logged in as "student3"
        Then I should see "Blue (1 votes)"
        And I should see "Green (1 votes)"
        And I should see "Red (0 votes)"
