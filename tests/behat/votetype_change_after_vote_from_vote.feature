@mod @mod_vote @uon
Feature: Modifying vote type of Vote (after voting)
    When the vote is closed
    As an editing teacher
    I should be able to modify the vote type

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | teacher@example.com |
            | student1 | Student | 1 | student1@example.com |
            | student2 | Student | 2 | student2@example.com |
            | student3 | Student | 3 | student3@example.com |
            | student4 | Student | 4 | student4@example.com |
            | student5 | Student | 5 | student4@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |
            | student1 | C1 | student |
            | student2 | C1 | student |
            | student3 | C1 | student |
            | student4 | C1 | student |
            | student5 | C1 | student |
        # votetype 2 is a vote, the close date is set to the earliest possible timestamp.
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate | closedate |
            | vote | C1 | vote1 | AV test | The cake is a lie! | 2 | 1 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | Which desert do you want? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | Which desert do you want? | Cake |
            | vote1 | Which desert do you want? | Lie |
            | vote1 | Which desert do you want? | Pie |
        And the following vote "votes" exist:
            | vote | question | user | optionname |
            | vote1 |  Which desert do you want? | student1 | Cake |
            | vote1 |  Which desert do you want? | student2 | Pie |
            | vote1 |  Which desert do you want? | student3 | Pie |
            | vote1 |  Which desert do you want? | student4 | Lie |
            | vote1 |  Which desert do you want? | student5 | Cake |

    Scenario Outline: Changing vote type
        Given I am on the "AV test" "mod_vote > View" page logged in as "teacher1"
        And I should see "Cake (2 votes)"
        And I should see "Lie (1 votes)"
        And I should see "Pie (2 votes)"
        When I navigate to "Settings" in current page administration
        And I set the field "<fieldname>" to "<newtype>"
        And I press "submitbutton"
        Then I should see "Cake (2 votes)"
        And I should see "Lie (1 votes)"
        And I should see "Pie (2 votes)"

        Examples:
            | fieldname | newtype |
            | id_votetype_1 | Poll |
            | id_votetype_3 | Alternative |
