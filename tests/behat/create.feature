@mod @mod_vote @uon
Feature: Vote activity creation
    In order to use the activity
    As an editing teacher
    I need to be create a vote

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | testteacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |

    @javascript
    Scenario: Create a poll
        Given I log in as "teacher1"
        And I am on "Course 1" course homepage with editing mode on
        And I add a "Vote" to section "1" using the activity chooser
        And I set the following fields to these values:
            | Vote name | The title of my vote |
            | Description | Description of the vote activity I will create |
            | id_votetype_1 | Poll |
            | Vote active | No |
        And I press "Save and display"
        And I am on the "The title of my vote" "mod_vote > View" page
        Then I should see "Vote type: Poll"
        But I should not see "Vote type: Vote"
        And I should not see "Vote type: Alternative vote"

    @javascript
    Scenario: Create a poll
        Given I log in as "teacher1"
        When I add a "vote" activity to course "Course 1" section "1" and I fill the form with:
            | Vote name | The title of my vote |
            | Description | Description of the vote activity I will create |
            | id_votetype_2 | Vote |
            | Vote active | No |
        And I am on the "The title of my vote" "mod_vote > View" page
        Then I should see "Vote type: Vote"
        But I should not see "Vote type: Poll"
        And I should not see "Vote type: Alternative vote"

    @javascript
    Scenario: Create a poll
        Given I log in as "teacher1"
        When I add a "vote" activity to course "Course 1" section "1" and I fill the form with:
            | Vote name | The title of my vote |
            | Description | Description of the vote activity I will create |
            | id_votetype_3 | Alternative vote |
            | Vote active | No |
        And I am on the "The title of my vote" "mod_vote > View" page
        Then I should see "Vote type: Alternative vote"
        But I should not see "Vote type: Poll"
        And I should not see "Vote type: Vote"

    @javascript
    Scenario: Message when saving a vote without completing mandatory field
        Given I log in as "teacher1"
        And I am on "Course 1" course homepage with editing mode on
        And I add a "vote" to section "1" using the activity chooser
        When I click on "submitbutton" "button"
        Then I should see "You must supply a value here"
