<?php
// This file is part of the Vote activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote\external;

use core_external\external_api;

/**
 * Tests the vote voting web service.
 *
 * @package     mod_vote
 * @category    test
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @runTestsInSeparateProcesses
 * @covers \mod_vote\external\vote
 * @group mod_vote
 * @group uon
 */
final class vote_test extends \advanced_testcase {
    /**
     * Tests that voting for a poll works correctly.
     *
     * @covers \mod_vote\external\vote
     * @group mod_vote
     * @group uon
     */
    public function test_vote_poll(): void {
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $vote = $votegenerator->create_instance(['course' => $course->id, 'votetype' => VOTE_TYPE_POLL]);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);
        // Build the request.
        $voterequest = [
            [
                'question' => $question->id,
                'options' => [
                    [
                        'id' => $question->options[1]->id,
                        'rank' => 1, // Voted for.
                    ],
                ],
            ],
        ];
        // Call the web service.
        self::setUser($user1);
        $USER->ignoresesskey = true;
        $args = [
                'id' => $vote->id,
                'type' => $vote->votetype,
                'vote' => $voterequest,
        ];
        $result = external_api::call_external_function('mod_vote_vote', $args);
        $expectedresult = [
            'status' => true,
            'warnings' => [],
        ];
        $this->assertEquals($expectedresult, $result['data']);
        // Test that the vote was actually recorded.
        $params = [
            'voteid' => $vote->id,
            'userid' => $user1->id,
        ];
        $this->assertTrue($DB->record_exists('vote_votes', $params));
        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that when no options are passed things work correctly.
     */
    public function test_vote_poll_no_option(): void {
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $vote = $votegenerator->create_instance(['course' => $course->id, 'votetype' => VOTE_TYPE_POLL]);
        $question = $votegenerator->create_question(
            $vote,
            ['question' => 'Test question'],
            [
                ['optionname' => 'First option'],
                ['optionname' => 'Second option'],
            ]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);
        // Build the request.
        $voterequest = [
            [
                'question' => $question->id,
            ],
        ];
        // Call the web service.
        self::setUser($user1);
        $USER->ignoresesskey = true;

        $args = [
                'id' => $vote->id,
                'type' => $vote->votetype,
                'vote' => $voterequest,
        ];
        $result = external_api::call_external_function('mod_vote_vote', $args);

        $expectedresult = [
            'status' => false,
            'warnings' => [
                [
                    'item' => 'question',
                    'itemid' => $question->id,
                    'warningcode' => '6',
                    'message' => 'Too few rankings',
                ],
            ],
        ];
        $this->assertEquals($expectedresult, $result['data']);
        // Test that no vote was recorded.
        $params = [
            'voteid' => $vote->id,
            'userid' => $user1->id,
        ];
        $this->assertFalse($DB->record_exists('vote_votes', $params));
        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that voting in a vote works correctly.
     *
     * @covers \mod_vote\external\vote::execute
     * @group mod_vote
     * @group uon
     */
    public function test_vote_vote(): void {
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $vote = $votegenerator->create_instance(['course' => $course->id, 'votetype' => VOTE_TYPE_VOTE]);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);
        // Build the request.
        $voterequest = [
            [
                'question' => $question->id,
                'options' => [
                    [
                        'id' => $question->options[0]->id,
                        'rank' => 1, // Voted for.
                    ],
                ],
            ],
        ];
        // Call the web service.
        self::setUser($user1);
        $USER->ignoresesskey = true;

        $args = [
                'id' => $vote->id,
                'type' => $vote->votetype,
                'vote' => $voterequest,
        ];
        $result = external_api::call_external_function('mod_vote_vote', $args);
        $expectedresult = [
                'status' => true,
                'warnings' => [],
        ];

        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);

        // Test that the vote was actually recorded.
        $params = [
            'voteid' => $vote->id,
            'userid' => $user1->id,
        ];
        $this->assertTrue($DB->record_exists('vote_votes', $params));
        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that voting for an alternative vote works correctly.
     *
     * @covers \mod_vote\external\vote::execute
     *
     * @group mod_vote
     * @group uon
     */
    public function test_vote_av(): void {
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $vote = $votegenerator->create_instance(['course' => $course->id, 'votetype' => VOTE_TYPE_AV]);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);
        // Build the request.
        $voterequest = [
            [
                'question' => $question->id,
                'options' => [
                    [
                        'id' => $question->options[0]->id,
                        'rank' => 2, // Rank 2.
                    ],
                    [
                        'id' => $question->options[1]->id,
                        'rank' => 1, // Rank 1.
                    ],
                ],
            ],
        ];
        // Call the web service.
        self::setUser($user1);
        $args = [
                'id' => $vote->id,
                'type' => $vote->votetype,
                'vote' => $voterequest,
        ];
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;
        $result = external_api::call_external_function('mod_vote_vote', $args);

        $expectedresult = [
            'status' => true,
            'warnings' => [],
        ];

        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);

        // Test that the vote was actually recorded.
        $params = [
            'voteid' => $vote->id,
            'userid' => $user1->id,
        ];
        $this->assertTrue($DB->record_exists('vote_votes', $params));
        $this->assertDebuggingNotCalled();
    }
}
