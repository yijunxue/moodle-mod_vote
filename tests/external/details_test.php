<?php
// This file is part of the Vote activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote\external;

use core_external\external_api;
use core_external\util;

/**
 * Tests the vote voting web service.
 *
 * @package     mod_vote
 * @category    test
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @runTestsInSeparateProcesses
 * @covers \mod_vote\external\details
 * @group mod_vote
 * @group uon
 */
final class details_test extends \advanced_testcase {
    /**
     * The result when the poll is not set to be avaliable.
     *
     * @covers \mod_vote\external\details::view
     * @group mod_vote
     * @group uon
     */
    public function test_not_active(): void {
        // The result when the poll is not set to be available.
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $voteparams = ['course' => $course->id, 'votetype' => VOTE_TYPE_POLL, 'votestate' => VOTE_STATE_EDITING];
        $vote = $votegenerator->create_instance($voteparams);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);

        // Call the web service.
        self::setUser($user1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $vote->id];
        $result = external_api::call_external_function('mod_vote_details', $args);

        // Verify the result.
        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        list($voteintro, $voteintroformat) = util::format_text($vote->intro, $vote->introformat,
                $context, 'mod_vote', 'intro', null);
        $expectedresult = [
            'id' => (int) $vote->id,
            'title' => $vote->name,
            'intro' => $voteintro,
            'introformat' => $voteintroformat,
            'votetype' => $vote->votetype,
            'closedate' => $vote->closedate,
            'votestate' => VOTE_STATE_EDITING,
            'hasvoted' => false,
            'cansubmit' => false,
            'questionsvisibile' => false,
            'resultsvisible' => false,
            'questions' => [],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
        $this->assertDebuggingNotCalled();
    }

    /**
     * The result when the user has not voted, but the vote is open and avaliable.
     *
     * @covers \mod_vote\external\details::view
     * @group mod_vote
     * @group uon
     */
    public function test_active_no_vote(): void {
        // The result when the user has not voted, but the vote is open and avaliable.
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $voteparams = ['course' => $course->id, 'votetype' => VOTE_TYPE_POLL];
        $vote = $votegenerator->create_instance($voteparams);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);

        // Call the web service.
        self::setUser($user1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $vote->id];
        $result = external_api::call_external_function('mod_vote_details', $args);

        // Verify the result.
        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        list($voteintro, $voteintroformat) = util::format_text($vote->intro, $vote->introformat,
                $context, 'mod_vote', 'intro', null);
        $expectedresult = [
            'id' => (int) $vote->id,
            'title' => $vote->name,
            'intro' => $voteintro,
            'introformat' => $voteintroformat,
            'votetype' => $vote->votetype,
            'closedate' => $vote->closedate,
            'votestate' => VOTE_STATE_ACTIVE,
            'hasvoted' => false,
            'cansubmit' => true,
            'questionsvisibile' => true,
            'resultsvisible' => false,
            'questions' => [
                [
                    'id' => $question->id,
                    'question' => $question->question,
                    'options' => [
                        [
                            'id' => $question->options[0]->id,
                            'name' => $question->options[0]->optionname,
                        ],
                        [
                            'id' => $question->options[1]->id,
                            'name' => $question->options[1]->optionname,
                        ],
                    ],
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
        $this->assertDebuggingNotCalled();
    }

    /**
     * Test a vote that has not passed it's close date yet.
     *
     * @covers \mod_vote\external\details::view
     * @group mod_vote
     * @group uon
     */
    public function test_voted_not_closed(): void {
        // Test a vote that has not passed it's close date yet.
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $voteparams = ['course' => $course->id, 'votetype' => VOTE_TYPE_VOTE];
        $vote = $votegenerator->create_instance($voteparams);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        $votegenerator->create_votes($user1, $question, [$question->options[1]]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);

        // Call the web service.
        self::setUser($user1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $vote->id];
        $result = external_api::call_external_function('mod_vote_details', $args);

        // Verify the result.
        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        list($voteintro, $voteintroformat) = util::format_text($vote->intro, $vote->introformat,
                $context, 'mod_vote', 'intro', null);
        $expectedresult = [
            'id' => (int) $vote->id,
            'title' => $vote->name,
            'intro' => $voteintro,
            'introformat' => $voteintroformat,
            'votetype' => $vote->votetype,
            'closedate' => $vote->closedate,
            'votestate' => VOTE_STATE_ACTIVE,
            'hasvoted' => true,
            'cansubmit' => false,
            'questionsvisibile' => false,
            'resultsvisible' => false,
            'questions' => [],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
        $this->assertDebuggingNotCalled();
    }

    /**
     * Test when the vote has competed.
     *
     * @covers \mod_vote\external\details::view
     * @group mod_vote
     * @group uon
     */
    public function test_vote_closed(): void {
        // Test when the vote has competed.
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $voteparams = ['course' => $course->id, 'votetype' => VOTE_TYPE_VOTE, 'closedate' => (time() - 86400)];
        $vote = $votegenerator->create_instance($voteparams);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        $votegenerator->create_votes($user1, $question, [$question->options[1]]);
        $votegenerator->create_votes($user2, $question, [$question->options[0]]);
        $votegenerator->create_votes($user3, $question, [$question->options[1]]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);

        // Call the web service.
        self::setUser($user1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $vote->id];
        $result = external_api::call_external_function('mod_vote_details', $args);

        // Verify the result.
        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        list($voteintro, $voteintroformat) = util::format_text($vote->intro, $vote->introformat,
                $context, 'mod_vote', 'intro', null);
        $expectedresult = [
            'id' => (int) $vote->id,
            'title' => $vote->name,
            'intro' => $voteintro,
            'introformat' => $voteintroformat,
            'votetype' => $vote->votetype,
            'closedate' => $vote->closedate,
            'votestate' => VOTE_STATE_ACTIVE,
            'hasvoted' => true,
            'cansubmit' => false,
            'questionsvisibile' => false,
            'resultsvisible' => true,
            'questions' => [
                [
                    'id' => $question->id,
                    'question' => $question->question,
                    'maxresult' => 2,
                    'rounds' => 0,
                    'options' => [
                        [
                            'id' => $question->options[1]->id,
                            'name' => $question->options[1]->optionname,
                            'result' => 2,
                            'round' => 0,
                        ],
                        [
                            'id' => $question->options[0]->id,
                            'name' => $question->options[0]->optionname,
                            'result' => 1,
                            'round' => 0,
                        ],
                    ],
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
        $this->assertDebuggingNotCalled();
    }

    /**
     * Test a poll the user has voted in, but the close date has not passed.
     *
     * @covers \mod_vote\external\details::view
     * @group mod_vote
     * @group uon
     */
    public function test_poll_voted_not_closed(): void {
        // Test a poll the user has voted in, but the close date has not passed.
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $voteparams = ['course' => $course->id, 'votetype' => VOTE_TYPE_POLL, 'closedate' => (time() + 86400)];
        $vote = $votegenerator->create_instance($voteparams);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                ]);
        $votegenerator->create_votes($user1, $question, [$question->options[1]]);
        $votegenerator->create_votes($user2, $question, [$question->options[0]]);
        $votegenerator->create_votes($user3, $question, [$question->options[1]]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);

        // Call the web service.
        self::setUser($user1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $vote->id];
        $result = external_api::call_external_function('mod_vote_details', $args);

        // Verify the result.
        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        list($voteintro, $voteintroformat) = util::format_text($vote->intro, $vote->introformat,
                $context, 'mod_vote', 'intro', null);
        $expectedresult = [
            'id' => (int) $vote->id,
            'title' => $vote->name,
            'intro' => $voteintro,
            'introformat' => $voteintroformat,
            'votetype' => $vote->votetype,
            'closedate' => $vote->closedate,
            'votestate' => VOTE_STATE_ACTIVE,
            'hasvoted' => true,
            'cansubmit' => false,
            'questionsvisibile' => false,
            'resultsvisible' => true,
            'questions' => [
                [
                    'id' => $question->id,
                    'question' => $question->question,
                    'maxresult' => 2,
                    'rounds' => 0,
                    'options' => [
                        [
                            'id' => $question->options[1]->id,
                            'name' => $question->options[1]->optionname,
                            'result' => 2,
                            'round' => 0,
                        ],
                        [
                            'id' => $question->options[0]->id,
                            'name' => $question->options[0]->optionname,
                            'result' => 1,
                            'round' => 0,
                        ],
                    ],
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
        $this->assertDebuggingNotCalled();
    }

    /**
     * Test an alternative vote that has votes and is closed.
     *
     * @covers \mod_vote\external\details::view
     * @group mod_vote
     * @group uon
     */
    public function test_av_closed(): void {
        global $DB, $USER, $CFG;
        $this->resetAfterTest(true);
        require_once(dirname(__DIR__, 2) . '/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');
        // Create some test data.
        $studentrole = $DB->get_record('role', ['shortname' => 'student']);
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $user4 = self::getDataGenerator()->create_user();
        $user5 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $voteparams = ['course' => $course->id, 'votetype' => VOTE_TYPE_AV, 'closedate' => (time() - 86400)];
        $vote = $votegenerator->create_instance($voteparams);
        $question = $votegenerator->create_question(
                $vote,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                    ['optionname' => 'Third option'],
                ]);
        $votegenerator->create_votes($user1, $question, [$question->options[0], $question->options[1], $question->options[2]]);
        $votegenerator->create_votes($user2, $question, [$question->options[0], $question->options[2], $question->options[1]]);
        $votegenerator->create_votes($user3, $question, [$question->options[1], $question->options[0], $question->options[1]]);
        $votegenerator->create_votes($user4, $question, [$question->options[1], $question->options[2], $question->options[0]]);
        $votegenerator->create_votes($user5, $question, [$question->options[2], $question->options[0], $question->options[1]]);
        self::getDataGenerator()->enrol_user($user1->id, $course->id, $studentrole->id);
        self::getDataGenerator()->enrol_user($user2->id, $course->id, $studentrole->id);

        // Call the web service.
        self::setUser($user1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $vote->id];
        $result = external_api::call_external_function('mod_vote_details', $args);

        // Verify the result.
        $cm = get_coursemodule_from_instance('vote', $vote->id);
        $context = \context_module::instance($cm->id);
        list($voteintro, $voteintroformat) = util::format_text($vote->intro, $vote->introformat,
                $context, 'mod_vote', 'intro', null);
        $expectedresult = [
            'id' => (int) $vote->id,
            'title' => $vote->name,
            'intro' => $voteintro,
            'introformat' => $voteintroformat,
            'votetype' => $vote->votetype,
            'closedate' => $vote->closedate,
            'votestate' => VOTE_STATE_ACTIVE,
            'hasvoted' => true,
            'cansubmit' => false,
            'questionsvisibile' => false,
            'resultsvisible' => true,
            'questions' => [
                [
                    'id' => $question->id,
                    'question' => $question->question,
                    'maxresult' => 3,
                    'rounds' => 2,
                    'options' => [
                        [
                            'id' => $question->options[0]->id,
                            'name' => $question->options[0]->optionname,
                            'result' => 3,
                            'round' => 2,
                        ],
                        [
                            'id' => $question->options[1]->id,
                            'name' => $question->options[1]->optionname,
                            'result' => 2,
                            'round' => 2,
                        ],
                        [
                            'id' => $question->options[2]->id,
                            'name' => $question->options[2]->optionname,
                            'result' => 1,
                            'round' => 1,
                        ],
                    ],
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
        $this->assertDebuggingNotCalled();
    }
}
