<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_vote;

use stdClass;

/**
 * Tests the vote activities mod_vote_cachelib.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 * @group uon
 */
final class cachelib_test extends \advanced_testcase {
    /**
     * Tests that mod_vote_cachelib is able to retrive data correctly.
     *
     * @covers \mod_vote\cachelib::get_cached_results
     * @covers \mod_vote\cachelib::clear_cache
     * @group mod_vote
     * @group uon
     */
    public function test_get_cached_results(): void {
        global $DB;

        $this->resetAfterTest(true);

        require_once(dirname(__DIR__).'/lib.php');

        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        $user0 = self::getDataGenerator()->create_user();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $user4 = self::getDataGenerator()->create_user();

        // Create a course and add a vote activity to it.
        $course0 = self::getDataGenerator()->create_course();
        $vote0 = $votegenerator->create_instance(['course' => $course0->id, 'votetype' => VOTE_TYPE_POLL]);
        $question0 = $votegenerator->create_question(
                $vote0,
                ['question' => 'Test question'],
                [
                    ['optionname' => 'First option'],
                    ['optionname' => 'Second option'],
                    ['optionname' => 'Third option'],
                    ['optionname' => 'Forth option'],
                ]);

        // Enrol some users onto the course and get them to vote in the poll.
        self::getDataGenerator()->enrol_user($user0->id, $course0->id);
        self::getDataGenerator()->enrol_user($user1->id, $course0->id);
        self::getDataGenerator()->enrol_user($user2->id, $course0->id);
        self::getDataGenerator()->enrol_user($user3->id, $course0->id);
        self::getDataGenerator()->enrol_user($user4->id, $course0->id);

        $votegenerator->create_votes($user0, $question0, [$question0->options[2]]);
        $votegenerator->create_votes($user1, $question0, [$question0->options[0]]);
        $votegenerator->create_votes($user2, $question0, [$question0->options[0]]);
        $votegenerator->create_votes($user3, $question0, [$question0->options[1]]);
        $votegenerator->create_votes($user4, $question0, [$question0->options[3]]);

        // Get the cached results.
        $results = cachelib::get_cached_results(new vote($vote0->id));

        // Check that the results are correct.
        $this->assertTrue($results->valid());
        $this->validate_result(2, 0, $question0->options[0], $question0, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 0, $question0->options[3], $question0, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 0, $question0->options[1], $question0, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 0, $question0->options[2], $question0, $results->current());
        $results->next();
        $this->assertFalse($results->valid());
        $results->close();

        // Now test AV.
        $vote1 = $votegenerator->create_instance(['course' => $course0->id, 'votetype' => VOTE_TYPE_AV]);
        $question1 = $votegenerator->create_question(
                $vote1,
                ['question' => 'Test question 2'],
                [
                    ['optionname' => 'Option 1'],
                    ['optionname' => 'Option 4'],
                    ['optionname' => 'Option 3'],
                    ['optionname' => 'Option 2'],
                ]);

        $votegenerator->create_votes($user0, $question1, [$question1->options[0], $question1->options[2]]);
        $votegenerator->create_votes($user1, $question1, [$question1->options[1]]);
        $votegenerator->create_votes($user2, $question1, [$question1->options[2]]);
        $votegenerator->create_votes($user3, $question1, [$question1->options[3], $question1->options[2]]);
        $votegenerator->create_votes($user4, $question1, [$question1->options[1]]);

        // Get the cached results.
        $results = cachelib::get_cached_results(new vote($vote1->id));

        // Check that the results are correct.
        $this->assertTrue($results->valid());
        $this->validate_result(3, 3, $question1->options[2], $question1, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question1->options[1], $question1, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 2, $question1->options[3], $question1, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 1, $question1->options[0], $question1, $results->current());
        $results->next();
        $this->assertFalse($results->valid());
        $results->close();

        // Create an AV vote with two questions.
        $vote2 = $votegenerator->create_instance(['course' => $course0->id, 'votetype' => VOTE_TYPE_AV]);
        $question2 = $votegenerator->create_question(
                $vote2,
                ['question' => 'Test question 3'],
                [
                    ['optionname' => 'Option 1', 'sortorder' => 1], // Also testing sort order.
                    ['optionname' => 'Option 4', 'sortorder' => 4],
                    ['optionname' => 'Option 3', 'sortorder' => 3],
                    ['optionname' => 'Option 2', 'sortorder' => 2],
                ]);
        $question3 = $votegenerator->create_question(
                $vote2,
                ['question' => 'Test question 4'],
                [
                    ['optionname' => 'Option 1', 'sortorder' => 4], // Also testing sort order.
                    ['optionname' => 'Option 4', 'sortorder' => 1],
                    ['optionname' => 'Option 3', 'sortorder' => 2],
                    ['optionname' => 'Option 2', 'sortorder' => 3],
                ]);

        // Should give the same results as the previous test, even when votes are cast by different people.
        $votegenerator->create_votes($user1, $question2, [$question2->options[0], $question2->options[2]]);
        $votegenerator->create_votes($user0, $question2, [$question2->options[1], $question2->options[2], $question2->options[0]]);
        $votegenerator->create_votes($user2, $question2, [$question2->options[2], $question2->options[3]]);
        $votegenerator->create_votes($user4, $question2, [$question2->options[3], $question2->options[2]]);
        $votegenerator->create_votes($user3, $question2, [$question2->options[1], $question2->options[2]]);

        // Test when some options are not represented as a first choice.
        $votegenerator->create_votes($user0, $question3, [$question3->options[0], $question3->options[2]]);
        $votegenerator->create_votes($user1, $question3, [$question3->options[1]]);
        $votegenerator->create_votes($user2, $question3, [$question3->options[0]]);
        $votegenerator->create_votes($user3, $question3, [$question3->options[3], $question3->options[2]]);
        $votegenerator->create_votes($user4, $question3, [$question3->options[1]]);

        // Get the cached results.
        $results = cachelib::get_cached_results(new vote($vote2->id));

        // Check that the results are correct.
        $this->assertTrue($results->valid());
        $this->validate_result(3, 3, $question2->options[2], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question2->options[1], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 2, $question2->options[3], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 1, $question2->options[0], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question3->options[1], $question3, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question3->options[0], $question3, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 2, $question3->options[3], $question3, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(0, 1, $question3->options[2], $question3, $results->current());
        $results->next();
        $this->assertFalse($results->valid());
        $results->close();

        // Test that clearing the cache works, first count the records present.
        $cacherecords = $DB->count_records('vote_result_cache');
        $vote2cahcherecords = $DB->count_records('vote_result_cache', ['voteid' => $vote2->id]);
        $this->assertGreaterThan(0, $DB->count_records('vote_result_cache', ['voteid' => $vote2->id]));

        cachelib::clear_cache($vote2->id);
        $this->assertEquals(0, $DB->count_records('vote_result_cache', ['voteid' => $vote2->id]));
        $this->assertEquals(($cacherecords - $vote2cahcherecords), $DB->count_records('vote_result_cache'));

        $this->assertDebuggingNotCalled();
    }

    /**
     * Test that the cached result is as expected.
     *
     * @param int $expectedresult - The number of votes we expect the option to have.
     * @param int $expectedround - The round number we expect the result to have.
     * @param \stdClass $expectedoption - The option we expect this result to be for.
     * @param \stdClass $expectedquestion - The question we expect this result to be for.
     * @param \stdClass $result - The result we are testing.
     */
    protected function validate_result($expectedresult, $expectedround, stdClass $expectedoption, stdClass $expectedquestion, stdClass $result): void {
        $this->assertEquals($expectedoption->optionname, $result->optionname);
        $this->assertEquals($expectedoption->id, $result->oid);
        $this->assertEquals($expectedquestion->id, $result->qid);
        $this->assertEquals($expectedquestion->question, $result->question);
        $this->assertEquals($expectedresult, $result->result);
        $this->assertEquals($expectedround, $result->round);
    }
}
