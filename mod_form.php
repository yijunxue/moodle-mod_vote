<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * The main vote configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_vote_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {

        $mform = $this->_form;

        // Adding the "general" fieldset, where all the common settings are showed.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('votename', 'mod_vote'), ['size' => '64']);
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'votename', 'mod_vote');

        // Adding the standard "intro" and "introformat" fields.
        moodleform_mod::standard_intro_elements();

        // Set the close date of the vote.
        $mform->addElement('date_time_selector', 'closedate', get_string('closedate', 'mod_vote'));
        $mform->addHelpButton('closedate', 'closedate', 'mod_vote');
        $mform->setDefault('closedate', time() + 1209600); // Sets the default time to be in 2 weeks.
        $mform->setType('closedate', PARAM_INT);

        // Set the type of vote.
        $radioarray = [];
        $radioarray[] = $mform->createElement('radio', 'votetype', '',
                get_string('type_poll', 'mod_vote'), VOTE_TYPE_POLL, '');
        $radioarray[] = $mform->createElement('radio', 'votetype', '',
                get_string('type_vote', 'mod_vote'), VOTE_TYPE_VOTE, '');
        $radioarray[] = $mform->createElement('radio', 'votetype', '',
                get_string('type_av', 'mod_vote'), VOTE_TYPE_AV, '');
        $mform->addGroup($radioarray, 'type', get_string('type', 'mod_vote'),
                [html_writer::empty_tag('br')], false);
        $mform->addHelpButton('type', 'type', 'mod_vote');
        $mform->setDefault('votetype', VOTE_TYPE_POLL);
        $mform->setType('votetype', PARAM_INT);

        $mform->addElement('selectyesno', 'votestate', get_string('votestate', 'mod_vote'));
        $mform->addHelpButton('votestate', 'votestate', 'mod_vote');
        $mform->setDefault('votestate', 0);

        // Add standard elements, common to all modules.
        $this->standard_coursemodule_elements();
        // Add standard buttons, common to all modules.
        $this->add_action_buttons();
    }

    /**
     * Determines if a completion rule has been specified.
     *
     * @param mixed[] $data
     * @return bool
     */
    public function completion_rule_enabled($data) {
        return !empty($data['completionvoted']) || !empty($data['completionviewresults']);
    }

    /**
     * Adds custom completion rules to the form.
     *
     * @return string[] - array containing the element names added.
     */
    public function add_completion_rules() {
        $mform =& $this->_form;
        $mform->addElement('checkbox', 'completionvoted', '', get_string('completionvoted', 'vote'));
        // Enable this completion rule by default.
        $mform->setDefault('completionvoted', 1);
        return ['completionvoted'];

    }

    /**
     * The form's post processing to check statues
     *
     * @see moodleform_mod::data_postprocessing
     */
    public function data_postprocessing($data) {
        $autocompletion = (!empty($data->completion) && $data->completion == COMPLETION_TRACKING_AUTOMATIC);
        if (empty($data->completionvoted) || !$autocompletion) {
            $data->completionvoted = 0;
        }
    }
}
